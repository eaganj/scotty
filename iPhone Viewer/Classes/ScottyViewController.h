//
//  ScottyViewController.h
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright James R. Eagan 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScottyImageReceiver.h"
#import "ScottyImageView.h"

//#define SERVER_HOST @"jagaroth.local" // Won't work -- the phone doesn't seem to find this host
//#define SERVER_HOST @"127.0.0.1" // Won't work -- 127.0.0.1 is the phone's localhost
#define SERVER_HOST @"129.175.11.245" // me wired
//#define SERVER_HOST @"129.175.22.98" // Clemens
//#define SERVER_HOST @"129.175.22.228" // me (James)
//#define SERVER_HOST @"192.168.0.1" // WILD room frontal
//#define SERVER_HOST @"192.168.0.69" // WILD room
#define SERVER_PORT 9889


@interface ScottyViewController : UIViewController {
    ScottyImageReceiver *imageReceiver;
    IBOutlet ScottyImageView *imageView;
    NSString *host;
    NSInteger port;
}

@property (readonly) ScottyImageReceiver *imageReceiver;
@property (nonatomic, retain) NSString *host;
@property (readwrite) NSInteger port;

- (id)initWithHost:(NSString *)host port:(NSInteger)port;
- (void)setRemoteImage:(UIImage *)image;

@end

