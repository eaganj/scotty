//
//  JREGeomNSTransformAdditions.m
//  Scotty
//
//  Created by James Eagan on 09/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import "JREGeomNSTransformAdditions.h"


@implementation NSAffineTransform (JREGeomAdditions)
- (NSRect)transformRect:(NSRect)rect
{
    NSPoint origin = [self transformPoint:rect.origin];
    NSSize size = [self transformSize:rect.size];
    //return NSMakeRect(origin.x, origin.y, size.width, size.height);
    return NSRectNormalize(NSMakeRect(origin.x, origin.y, size.width, size.height));
}

- (CGRect)transformCGRect:(CGRect)rect
{
    return NSRectToCGRect([self transformRect:NSRectFromCGRect(rect)]);
}

- (CGPoint)transformCGPoint:(CGPoint)point
{
    return NSPointToCGPoint([self transformPoint:NSPointFromCGPoint(point)]);
    NSPoint result = [self transformPoint:*(NSPoint*)&point];
    return *(CGPoint*)&result;
}

- (CGSize)transformCGSize:(CGSize)size
{
    return NSSizeToCGSize([self transformSize:NSSizeFromCGSize(size)]);
    NSSize result = [self transformSize:*(NSSize*)&size];
    return *(CGSize*)&result;
}

@end


NSRect NSRectNormalize(NSRect rect) {
    if (rect.size.width < 0.0) {
        rect.origin.x += rect.size.width;
        rect.size.width = -rect.size.width;
    }
    if (rect.size.height < 0.0) {
        rect.origin.y += rect.size.height;
        rect.size.height = -rect.size.height;
    }
    return rect;
}