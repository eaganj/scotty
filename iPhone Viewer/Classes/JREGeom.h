//
//  JREGeom.h
//  Scotty
//
//  Created by James Eagan on 08/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JREAffineTransform.h"
//#import "JREGeomNSTransformAdditions.h"

@interface JREGeom : NSObject {

}

+ (CGRect)centerRect:(CGRect)inner inRect:(CGRect)outer;
+ (CGRect)scaledRect:(CGRect)inner toFitInRect:(CGRect)outer;
+ (CGRect)croppedRect:(CGRect)inner toFitInRect:(CGRect)outer;

+ (JREAffineTransform *)transformForCenteredRect:(CGRect)inner inRect:(CGRect)outer;
+ (JREAffineTransform *)transformForScaledRect:(CGRect)inner toFitInRect:(CGRect)outer;
+ (JREAffineTransform *)transformForCroppedRect:(CGRect)inner toFitInRect:(CGRect)outer;

@end