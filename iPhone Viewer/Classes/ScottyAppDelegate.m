//
//  ScottyAppDelegate.m
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright James R. Eagan 2009. All rights reserved.
//

#import "ScottyAppDelegate.h"
#import "ScottyViewController.h"

@implementation ScottyAppDelegate

@synthesize window;
@synthesize viewController;
@synthesize bonjourBrowser;

#define kScottyServiceType @"_scotty._tcp"
#define kInitialDomain @"local"

- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    //[window addSubview:viewController.view];
    //[window makeKeyAndVisible];
    
    BonjourBrowser *browser = [[BonjourBrowser alloc] initForType:kScottyServiceType 
                                                         inDomain:kInitialDomain 
                                                    customDomains:nil
                                         showDisclosureIndicators:NO 
                                                 showCancelButton:NO];
    
    self.bonjourBrowser = browser;
    [browser release];
    
    self.bonjourBrowser.delegate = self;
    self.bonjourBrowser.searchingForServicesString = @"Searching for Scotty Servers...";
    [self.window addSubview:[self.bonjourBrowser view]];
}


- (void)dealloc {
    [bonjourBrowser release];
    //[viewController release];
    [window release];
    [super dealloc];
}

- (NSString *)copyStringFromTXTDict:(NSDictionary *)dict which:(NSString*)which {
	// Helper for getting information from the TXT data
	NSData* data = [dict objectForKey:which];
	return data ? [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease] : nil;
}


- (void) bonjourBrowser:(BonjourBrowser*)browser didResolveInstance:(NSNetService*)service {
	// Construct the URL including the port number
	// Also use the path, username and password fields that can be in the TXT record
    
    NSString *host = [service hostName];
    NSInteger port = [service port];
    [[self.bonjourBrowser view] removeFromSuperview];
    
    ScottyViewController *scottyView = [[ScottyViewController alloc] initWithHost:host port:port];
    self.viewController = scottyView;
    [scottyView release];
    
    [[self.viewController view] setFrame:[[UIScreen mainScreen] applicationFrame]];
    [self.window addSubview:[self.viewController view]];
    //[window addSubview:viewController.view];
    
    /*
	NSDictionary* dict = [[NSNetService dictionaryFromTXTRecordData:[service TXTRecordData]] retain];
	NSString *host = [service hostName];
	
	NSString* user = [[self copyStringFromTXTDict:dict which:@"u"] retain];
	NSString* pass = [[self copyStringFromTXTDict:dict which:@"p"] retain];
	
	NSString* portStr = [[NSString alloc] initWithString:@""];
	
	// Note that [NSNetService port:] returns an NSInteger in host byte order
	NSInteger port = [service port];
	if (port != 0 && port != 80)
        portStr = [[NSString alloc] initWithFormat:@":%d",port];
	
	NSString* path = [[[self copyStringFromTXTDict:dict which:@"path"] retain] autorelease];
	if (!path || [path length]==0) {
        path = [[[NSString alloc] initWithString:@"/"] autorelease];
	} else if (![[path substringToIndex:1] isEqual:@"/"]) {
        path = [[[NSString alloc] initWithFormat:@"/%@",path] autorelease];
	}
	
	NSString* string = [[NSString alloc] initWithFormat:@"http://%@%@%@%@%@%@%@",
                        user?user:@"",
                        pass?@":":@"",
                        pass?pass:@"",
                        (user||pass)?@"@":@"",
                        host,
                        portStr,
                        path];
	
	NSURL *url = [[NSURL alloc] initWithString:string];
	[[UIApplication sharedApplication] openURL:url];
	
	[url release];
	[string release];
	[portStr release];
	[pass release];
	[user release];
	[dict release];
     */
}


@end
