//
//  JREGeomNSTransformAdditions.h
//  Scotty
//
//  Created by James Eagan on 09/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSAffineTransform (JREGeomAdditions)
- (NSRect)transformRect:(NSRect)rect;
- (CGRect)transformCGRect:(CGRect)rect;

- (CGPoint)transformCGPoint:(CGPoint)point;
- (CGSize)transformCGSize:(CGSize)size;
@end

NSRect NSRectNormalize(NSRect rect);