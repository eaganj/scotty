//
//  JREAffineTransform.m
//  Scotty
//
//  Created by James Eagan on 16/09/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import "JREAffineTransform.h"


@implementation JREAffineTransform

+ (id)transform
{
    return [[[JREAffineTransform alloc] init] autorelease];
}

- (id)init
{
    self = [super init];
    if (!self) {
        return self;
    }
    
    _tf = CGAffineTransformMakeScale(1.0, 1.0);
    return self;
}

- (id)initWithTransform:(JREAffineTransform*)transform {
    self = [super init];
    if (!self) {
        return self;
    }
    
    _tf = transform->_tf;
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    JREAffineTransform *result = [[self class] transform];
    result->_tf = _tf;
    return result;
}

- (void)translateXBy:(float)dx yBy:(float)dy
{
    _tf = CGAffineTransformTranslate(_tf, dx, dy);
}

- (void)rotateByDegrees:(float)deg
{
    _tf = CGAffineTransformRotate(_tf, deg * 0.0174532925);
}

- (void)rotateByRadians:(float)rad
{
    _tf = CGAffineTransformRotate(_tf, rad);
}

- (void)scaleBy:(float)scaleFactor
{
    _tf = CGAffineTransformScale(_tf, scaleFactor, scaleFactor);
}

- (void)scaleXBy:(float)xFactor yBy:(float)yFactor
{
    _tf = CGAffineTransformScale(_tf, xFactor, yFactor);
}

- (void)invert
{
    _tf = CGAffineTransformInvert(_tf);
}

- (void)appendTransform:(JREAffineTransform*)transform
{
    _tf = CGAffineTransformConcat(_tf, transform->_tf);
}

- (void)prependTransform:(JREAffineTransform*)transform
{
    _tf = CGAffineTransformConcat(transform->_tf, _tf);
}

- (struct CGPoint)transformPoint:(struct CGPoint)point
{
    return CGPointApplyAffineTransform(point, _tf);
}

- (struct CGSize)transformSize:(struct CGSize)size
{
    return CGSizeApplyAffineTransform(size, _tf);
}

- (struct CGRect)transformRect:(struct CGRect)rect
{
    return CGRectApplyAffineTransform(rect, _tf);
}

- (struct CGPoint)transformCGPoint:(struct CGPoint)point
{
    return CGPointApplyAffineTransform(point, _tf);
}

- (struct CGSize)transformCGSize:(struct CGSize)size
{
    return CGSizeApplyAffineTransform(size, _tf);
}

- (struct CGRect)transformCGRect:(struct CGRect)rect
{
    return CGRectApplyAffineTransform(rect, _tf);
}

@end
