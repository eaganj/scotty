//
//  ScottyViewController.m
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright James R. Eagan 2009. All rights reserved.
//

#import "ScottyViewController.h"

@implementation ScottyViewController

@synthesize imageReceiver;
@synthesize host;
@synthesize port;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

- (id)initWithHost:(NSString *)host port:(NSInteger)port {
    self = [super initWithNibName:@"ScottyViewController" bundle:nil];
    if (!self) {
        return nil;
    }
    
    self.host = host;
    self.port = port;
    self.wantsFullScreenLayout = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES animated:YES];
    
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    imageReceiver = [[ScottyImageReceiver alloc] initWithDelegate:self 
                                                             host:SERVER_HOST 
                                                             port:SERVER_PORT];
    imageView.delegate = imageReceiver;
    [imageReceiver run];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES; //(interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}

- (void)setRemoteImage:(UIImage *)image
{
    imageView.image = image;
}


@end
