//
//  JREGeom.m
//  Scotty
//
//  Created by James Eagan on 08/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import "JREGeom.h"


@implementation JREGeom

+(CGRect) centerRect:(CGRect)inner inRect:(CGRect)outer
{
    return CGRectMake(outer.origin.x + (outer.size.width - inner.size.width) / 2.0, 
                      outer.origin.y + (outer.size.height - inner.size.height) / 2.0, 
                      inner.size.width, inner.size.height);
}

+(CGRect) scaledRect:(CGRect)inner toFitInRect:(CGRect)outer
{
    CGFloat scale = MIN(outer.size.width / inner.size.width, outer.size.height / inner.size.height);
    CGRect scaled = CGRectMake(outer.origin.x, outer.origin.y,
                               scale * inner.size.width,
                               scale * inner.size.height);
    return [JREGeom centerRect:scaled inRect:outer];
}

+(CGRect) croppedRect:(CGRect)inner toFitInRect:(CGRect)outer
{
    return CGRectIntersection(inner, outer);
}

+ (JREAffineTransform *)transformForCenteredRect:(CGRect)inner inRect:(CGRect)outer
{
    CGFloat dx = (outer.size.width - inner.size.width) / 2.0;
    CGFloat dy = (outer.size.height - inner.size.height) / 2.0;
    JREAffineTransform *xform = [JREAffineTransform transform];
    [xform translateXBy:dx yBy:dy];
    return xform;
}

+ (JREAffineTransform *)transformForScaledRect:(CGRect)inner toFitInRect:(CGRect)outer
{
    CGFloat scale = MIN(outer.size.width / inner.size.width, outer.size.height / inner.size.height);
    JREAffineTransform *xform = [JREAffineTransform transform];
    [xform scaleBy:scale];
    //NSPoint origin = [xform transformPoint:*(NSPoint*)&inner.origin];
    //NSSize size = [xform transformSize:*(NSSize*)&inner.size];
//    CGRect scaled = CGRectMake(origin.x, origin.y, size.width, size.height);
    CGRect scaled = [xform transformCGRect:inner];
    [xform appendTransform:[JREGeom transformForCenteredRect:scaled inRect:outer]];
    return xform;
}

+ (JREAffineTransform *)transformForCroppedRect:(CGRect)inner toFitInRect:(CGRect)outer
{
    return nil;
}

@end