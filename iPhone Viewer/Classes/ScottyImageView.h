//
//  ScottyImageView.h
//  Scotty
//
//  Created by James Eagan on 06/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JREAffineTransform.h"


/* We might be tempted to make ScottyImageView a subclass of UIImageView, but don't do it.
   The cake is a lie.  It turns out that UIImageViews do some funky juju and do not call the
   drawRect: method.  No big deal, it's easy enough to draw the image ourselves.  Just resist
   any temptation to extend UIImageView.
 */
@interface ScottyImageView : UIView {
    UIImage *image;
    JREAffineTransform *outputTransform;
    JREAffineTransform *inputTransform;
    
    id delegate;
}

@property (readwrite, retain) UIImage *image;
@property (readwrite, retain) id delegate;

- (void)updateTransforms;

@end
