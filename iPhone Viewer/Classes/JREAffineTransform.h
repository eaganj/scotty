//
//  JREAffineTransform.h
//  Scotty
//
//  Created by James Eagan on 16/09/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JREAffineTransform : NSObject {
    CGAffineTransform _tf;
}

+ (id)transform;
- (id)init;
- (id)initWithTransform:(JREAffineTransform*)transform;
- (id)copyWithZone:(NSZone *)zone;
- (void)translateXBy:(float)dx yBy:(float)dy;
- (void)rotateByDegrees:(float)deg;
- (void)rotateByRadians:(float)rad;
- (void)scaleBy:(float)scaleFactor;
- (void)scaleXBy:(float)xFactor yBy:(float)yFactor;
- (void)invert;
- (void)appendTransform:(JREAffineTransform*)transform;
- (void)prependTransform:(JREAffineTransform*)transform;
- (struct CGPoint)transformPoint:(struct CGPoint)point;
- (struct CGSize)transformSize:(struct CGSize)size;
- (struct CGRect)transformRect:(struct CGRect)size;

- (struct CGPoint)transformCGPoint:(struct CGPoint)point;
- (struct CGSize)transformCGSize:(struct CGSize)size;
- (struct CGRect)transformCGRect:(struct CGRect)size;

@end
