//
//  ScottyAppDelegate.h
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright James R. Eagan 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BonjourBrowser.h"

@class ScottyViewController;

@interface ScottyAppDelegate : NSObject <UIApplicationDelegate, BonjourBrowserDelegate> {
    UIWindow *window;
    ScottyViewController *viewController;
    BonjourBrowser *bonjourBrowser;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ScottyViewController *viewController;
@property (nonatomic, retain) BonjourBrowser *bonjourBrowser;


- (NSString *)copyStringFromTXTDict:(NSDictionary *)dict which:(NSString*)which; //copyStringFromTXTDict:which:
- (void) bonjourBrowser:(BonjourBrowser*)browser didResolveInstance:(NSNetService*)service;

@end

