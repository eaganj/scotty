//
//  ScottyImageReceiver.h
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <VVOSC/VVOSC.h>

#define WINDOW_CREATE_MSG 0x21
#define WINDOW_UPDATE_MSG 0x22
#define WINDOW_UNMAP_MSG 0x23
#define WINDOW_DESTROY_MSG 0x24

#define TAP_EVENT_MSG 0x1
#define TOUCH_DOWN_EVENT_MSG 0x2
#define TOUCH_MOVED_EVENT_MSG 0x3
#define TOUCH_UP_EVENT_MSG 0x4

#define ACK_IMAGE_MSG 0xf1

#define WINDOW_LIST_REQUEST_MSG 0x30
#define SUBSCRIBE_WINDOW_EVENTS_MSG 0x31
#define UNSUBSCRIBE_WINDOW_EVENTS_MSG 0x32
#define SUBSCRIBE_WINDOW_MSG 0x33
#define UNSUBSCRIBE_WINDOW_MSG 0x34

#define BEGIN_WINDOW_LIST_MSG 0x40
#define WINDOW_LIST_ITEM_MSG 0x41
#define END_WINDOW_LIST_MSG 0x42


typedef struct {
    uint32_t msg_type;
    CGFloat x;
    CGFloat y;
} TapEventMessage;

@interface ScottyImageReceiver : NSObject {
    
    NSHost *host;
    NSInteger port;
    id delegate;
    
    NSInputStream *inStream;
    NSOutputStream *outStream;
    
    NSMutableData *_data;    
    NSInteger desiredSize;
    NSInteger msgID;
    NSInteger windowID;
    
    OSCManager *oscManager;
    OSCOutPort *oscOut;
}

- (id) initWithDelegate:(id)delegate host:(NSString *)hostname port:(NSInteger)port;
- (void) run;
- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode;
//- (void)sendTapEventAtX:(CGFloat)x y:(CGFloat)y;
- (void)sendTouchDownEventAtX:(CGFloat)x y:(CGFloat)y;
- (void)sendTouchMovedEventAtX:(CGFloat)x y:(CGFloat)y;
- (void)sendTouchUpEventAtX:(CGFloat)x y:(CGFloat)y;
- (void)sendImageAck;

@end
