//
//  ScottyImageView.m
//  Scotty
//
//  Created by James Eagan on 06/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import "ScottyImageView.h"
#import "ScottyImageReceiver.h"
#import "JREGeom.h"


@implementation ScottyImageView

@synthesize image;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}

/*- (id)initWithImage:(UIImage *)image {
    if (self = [super initWithImage:image]) {
        NSLog(@"init with image");
    }
    
    return self;
}
*/

/*
- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
    [super dealloc];
}*/

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (image) {
        
        CGSize imageSize = [self.image size];
        CGRect imageRect = CGRectMake(0, 0, imageSize.width, imageSize.height);    
        CGRect scaledImageRect = [outputTransform transformCGRect:imageRect];
        
        // Draw image
        [image drawInRect:scaledImageRect];
        
//        // Draw debugging info
//        [[UIColor redColor] set];
//        CGRect temp = [outputTransform transformCGRect:imageRect];
//        UIRectFrame(temp);
//        UIRectFrame(CGRectMake(temp.origin.x, temp.origin.y, 20.0, 20.0));
//        NSLog(@"scaled to %@", NSStringFromRect(NSRectFromCGRect(temp)));
    }
    
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if (image) {
        UITouch *touch = [touches anyObject];
        CGPoint loc = [inputTransform transformCGPoint:[touch locationInView:self]];
        
        if (delegate) {
            [(ScottyImageReceiver *)delegate sendTouchDownEventAtX:loc.x y:loc.y];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    if (image) {
        UITouch *touch = [touches anyObject];
        CGPoint loc = [inputTransform transformCGPoint:[touch locationInView:self]];
        
        if (delegate) {
            [(ScottyImageReceiver *)delegate sendTouchMovedEventAtX:loc.x y:loc.y];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if (image) {
        UITouch *touch = [touches anyObject];
        CGPoint loc = [inputTransform transformCGPoint:[touch locationInView:self]];
        
        if (delegate) {
            [(ScottyImageReceiver*)delegate sendTouchUpEventAtX:loc.x y:loc.y];
        }
    }
}


- (void)setImage:(UIImage *)newImage
{
    [newImage retain];
    [image release];
    image = newImage;
    
    if (image && newImage && CGSizeEqualToSize([image size], [newImage size])) {
        [self updateTransforms];
    }
    
    [self setNeedsDisplay];
}

- (void)updateTransforms
{
    CGSize imageSize = [image size];
    CGRect imageRect = CGRectMake(0, 0, imageSize.width, imageSize.height);
    
    if (outputTransform) {
        [outputTransform release];
    }
    if (inputTransform) {
        [inputTransform release];
    }
    
    
    outputTransform = [JREAffineTransform transform];
    [outputTransform retain];
    
    // The Mac origin is in the lower, left corner (like the way we learn in math class (and PDF)).
    // The iPod uses the standard graphics origin used by nearly every other system, so we need to
    // flip one of the coordinate spaces to match the other.  Here we flip the view and then
    // translate its origin from the top left of the flipped space to the bottom left.
    [outputTransform scaleXBy:1.0 yBy:-1.0]; // Flip
    [outputTransform appendTransform:[JREGeom transformForScaledRect:imageRect toFitInRect:[self bounds]]];
    [outputTransform translateXBy:0 yBy:-imageRect.size.height]; // Translate origin
    
    inputTransform = [outputTransform copy];
    [inputTransform retain];
    [inputTransform invert];
}

@end
