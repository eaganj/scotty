//
//  ScottyImageReceiver.m
//  Scotty
//
//  Created by James Eagan on 02/07/09.
//  Copyright 2009 James R. Eagan. All rights reserved.
//

#import "ScottyImageReceiver.h"
#import <arpa/inet.h>


@implementation ScottyImageReceiver

#define SERVER_OSC_PORT 1234

- (id)initWithDelegate:(id)newDelegate host:(NSString *)newHostName port:(NSInteger)newPort
{
    self = [super init];
    if (!self) {
        return self;
    }
    
    host = [NSHost hostWithName:newHostName];
    port = newPort;
    
    oscManager = [[OSCManager alloc] init];
    oscOut = [oscManager createNewOutputToAddress:newHostName atPort:SERVER_OSC_PORT];
    
    delegate = [newDelegate retain];
    
    return self;
}

- (void)dealloc
{
    [delegate release];
    [super dealloc];
}


- (void)run
{
    [NSStream getStreamsToHost:host port:port inputStream:&inStream outputStream:&outStream];
    [inStream retain];
    [outStream retain];
    [inStream setDelegate:self];
    [outStream setDelegate:self];
    [inStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inStream open];
    [outStream open];
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode
{
    switch (eventCode) {
        case NSStreamEventHasBytesAvailable:
        {
            if (!_data) {
                _data = [[NSMutableData data] retain];
            }
            
            uint8_t buf[4096];
            unsigned int len = 0;
            len = [(NSInputStream *)stream read:buf maxLength:4096];
            if (len) {
                [_data appendBytes:buf length:len];
            }
            if (!msgID && [_data length] >= sizeof(NSInteger)) {
                [_data getBytes:&msgID length:sizeof(NSInteger)];
                msgID = ntohl(msgID);
                [_data replaceBytesInRange:NSMakeRange(0, sizeof(NSInteger)) 
                                 withBytes:NULL length:0];
            } 

            // FIXME : Really really need  a proper parser
            if (msgID == WINDOW_UPDATE_MSG) {
                if (!desiredSize && [_data length] >= 2*sizeof(NSInteger)) {
                    [_data getBytes:&windowID length:sizeof(NSInteger)];
                    windowID = ntohl(windowID);
                    [_data getBytes:&desiredSize 
                              range:NSMakeRange(sizeof(NSInteger), sizeof(NSInteger))];
                    desiredSize = ntohl(desiredSize);
                    [_data replaceBytesInRange:NSMakeRange(0, 2*sizeof(NSInteger)) 
                                     withBytes:NULL length:0];
                    //NSLog(@"Will want %i bytes (swapped)", desiredSize);
                } else if (desiredSize && [_data length] >= desiredSize) {
                    NSData *imageData = [_data subdataWithRange:NSMakeRange(0, desiredSize)];
                    [_data replaceBytesInRange:NSMakeRange(0, desiredSize) withBytes:NULL length:0];
                    desiredSize = 0;
                    msgID = 0;
                    UIImage *image = [[UIImage alloc] initWithData:imageData];
                    [image autorelease];
                    //NSLog(@"Setting image: %@", image);
                    [self sendImageAck];
                    [delegate setRemoteImage:image];
                }
            } //else if (msgID == WINDOW_CREATE_MSG) {
              //  <#statements#>
            //}
            break;
        }
        case NSStreamEventEndEncountered:
        {
            [stream close];
            [stream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [stream release];
            stream = nil; // Does this need to check for inStream/outStream and nil those, too?
            break;
        }
        case NSStreamEventErrorOccurred:
        {
            NSError *error = [stream streamError];
            NSLog(@"Oops : %@", [error localizedDescription]);
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ignore" otherButtonTitles:nil];
            [alertView show];
            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView release];
}

- (void)sendTouchDownEventAtX:(CGFloat)x y:(CGFloat)y
{
//    OSCMessage *msg = [OSCMessage createWithAddress:@"/Button/Scotty"];
//    [msg addInt:1];
//    //[msg addInt:windowID];
//    [oscOut sendThisMessage:msg];
//    NSLog(@"Sent OSC Message");
    
    //NSLog(@"[received sendTapEventAtX:%f y:%f]", x, y);
    int msgType = htonl(TOUCH_DOWN_EVENT_MSG);
    int winID = htonl(windowID);
    NSSwappedFloat nx = NSSwapHostFloatToBig(x);
    NSSwappedFloat ny = NSSwapHostFloatToBig(y);
    [outStream write:(uint8_t *)&msgType maxLength:sizeof(TAP_EVENT_MSG)];
    [outStream write:(uint8_t *)&winID maxLength:sizeof(windowID)];
    [outStream write:(uint8_t *)&nx maxLength:sizeof(x)];
    [outStream write:(uint8_t *)&ny maxLength:sizeof(y)];    
}

- (void)sendTouchMovedEventAtX:(CGFloat)x y:(CGFloat)y
{
    //NSLog(@"[received sendTapEventAtX:%f y:%f]", x, y);
    int msgType = htonl(TOUCH_MOVED_EVENT_MSG);
    int winID = htonl(windowID);
    NSSwappedFloat nx = NSSwapHostFloatToBig(x);
    NSSwappedFloat ny = NSSwapHostFloatToBig(y);
    [outStream write:(uint8_t *)&msgType maxLength:sizeof(TAP_EVENT_MSG)];
    [outStream write:(uint8_t *)&winID maxLength:sizeof(windowID)];
    [outStream write:(uint8_t *)&nx maxLength:sizeof(x)];
    [outStream write:(uint8_t *)&ny maxLength:sizeof(y)];
    
}

- (void)sendTouchUpEventAtX:(CGFloat)x y:(CGFloat)y
{
//    OSCMessage *msg = [OSCMessage createWithAddress:@"/Button/Scotty"];
//    [msg addInt:0];
//    //[msg addInt:windowID];
//    [oscOut sendThisMessage:msg];
    
    //NSLog(@"[received sendTapEventAtX:%f y:%f]", x, y);
    int msgType = htonl(TOUCH_UP_EVENT_MSG);
    int winID = htonl(windowID);
    NSSwappedFloat nx = NSSwapHostFloatToBig(x);
    NSSwappedFloat ny = NSSwapHostFloatToBig(y);
    [outStream write:(uint8_t *)&msgType maxLength:sizeof(TAP_EVENT_MSG)];
    [outStream write:(uint8_t *)&winID maxLength:sizeof(windowID)];
    [outStream write:(uint8_t *)&nx maxLength:sizeof(x)];
    [outStream write:(uint8_t *)&ny maxLength:sizeof(y)];    
}

- (void)sendImageAck
{
    int msgType = htonl(ACK_IMAGE_MSG);
    int winID = htonl(windowID);
    [outStream write:(uint8_t *)&msgType maxLength:sizeof(ACK_IMAGE_MSG)];
    [outStream write:(uint8_t *)&winID maxLength:sizeof(winID)];
}

@end
