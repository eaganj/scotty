# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from AppKit import *
from Foundation import *
import objc

import jre.cocoa
import jre.debug

class ScottyPreferencesWindowController(NSWindowController):
    _userDefaultsController = objc.IBOutlet()
    
    def __new__(cls, defaults):
        return cls.alloc().initWithUserDefaults_(defaults)
    
    def initWithUserDefaults_(self, defaults):
        self = super(ScottyPreferencesWindowController, self).initWithWindowNibName_(u"Preferences Window")
        if not self:
            return self
        
        self.defaults = defaults
        
        return self
    
    def awakeFromNib(self):
        self._userDefaultsController._setDefaults_(self.defaults) # Warning: undocumented private API!

PreferencesWindowController = ScottyPreferencesWindowController