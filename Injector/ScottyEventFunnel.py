# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

import objc

from IIKit import *
from ScottyController import Scotty

from types import FunctionType, MethodType

IGNORENAMES = ('__module__', '__name__', '__doc__')

# def EventFunnelDecorator(cls):
#     ''' Decorator to funnel in event funnel methods '''
#     
#     dict = EventFunnelMethodHolder.__dict__
#     clsMethodNames = [ name for name, method in cls.__dict__ if isinstance(method, (FunctionType, MethodType)) ]
#     methods = [ method for name, method in dict.iteritems() if name not in IGNORENAMES 
#                                                         and not name.startswith('__')
#                                                         and isinstance(method, (FunctionType, MethodType)) 
#                                                         and name not in clsMethodNames ]
#     
#     objc.classAddMethods(cls, methods)
#     return cls

def StateMachineEventFunnel(cls):
    def handler(self, event):
        wrappedEvent, _ = InstrumentManager.sharedInstrumentManager().wrapEvent(event, 'fr.lri.insitu.Scotty')
        if not self.stateMachine.process_event(wrappedEvent):
            pass
            # print "Warning: discarding unhandled event:", wrappedEvent.__class__.__name__,\
            #       "in", self.stateMachine.current_state()
        
    return EventFunnel(cls, handler)

__eventFunnels = {}
def EventFunnel(cls, handler=None):
    ''' Make an EventFunnel for the specified class.
        
        An event funnel is a subclass of the specified class with event handler methods added to relay
        events through Scotty.
    '''
    
    # Remember generated funnel classes for re-use.
    if (cls, handler) in __eventFunnels:
        return __eventFunnels[(cls, handler)]
    
    def EventFunnelMeta(name, bases, dict):
        name = '%sScottyEventFunnel%s' % (cls.__name__, id(handler) if handler else '')
        # print "Creating class %s(%s) with %s items" % (name, ','.join(map(str, bases)), len(dict))
        newClass = type(name, bases, dict)
        __eventFunnels[(cls, handler)] = newClass
        return newClass
        
    if not handler:
        handler = lambda self, event: Scotty().handleEvent_(event)
            
    class EventFunnel(cls):
        __metaclass__ = EventFunnelMeta

        # Events from NSResponder in 10.6.2

        # Mouse events
        def mouseDown_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseDown_(event) # FIXME: need to properly resolve parent class

        def mouseDragged_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseDragged_(event)

        def mouseUp_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseUp_(event)

        def mouseMoved_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseMoved_(event)

        def mouseEntered_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseEntered_(event)

        def mouseExited_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).mouseExited_(event)

        def rightMouseDown_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).rightMouseDown_(event)

        def rightMouseDragged_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).rightMouseDragged_(event)

        def rightMouseUp_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).rightMouseUp_(event)

        def otherMouseDown_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).otherMouseDown_(event)

        def otherMouseDragged_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).otherMouseDragged_(event)

        def otherMouseUp_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).otherMouseUp_(event)

        #Responding to Key Events
        def keyDown_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).keyDown_(event)

        def keyUp_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).keyUp_(event)

        # def interpretKeyEvents_(self, event):
        #     if not handler(self, event):
        #         super(EventFunnel, self).interpretKeyEvents_(event)
        # 
        # def performKeyEquivalent_(self, event):
        #     if not handler(self, event):
        #         super(EventFunnel, self).performKeyEquivalent_(event)

        # def performMnemonic_(self, event):
        #     if not handler(self, event):
        #         super(EventFunnel, self).performMnemonic_(event)

        def flushBufferedKeyEvents_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).flushBufferedKeyEvents_(event)


        # Responding to Other Kinds of Events
        def cursorUpdate_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).cursorUpdate_(event)

        def flagsChanged_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).flagsChanged_(event)

        def tabletPoint_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).tabletPoint_(event)

        def tabletProximity_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).tabletProximity_(event)

        def helpRequested_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).helpRequested_(event)

        def scrollWheel_(self, event):
            if not handler(self, event):
                super(EventFunnel, self).scrollWheel_(event)
        
        # Standard Cocoa responder actions
        def cancelOperation_(self, sender):
            if not handler(self, ('cancelOperation', sender)):
                super(EventFunnel, self).cancelOperation_(sender)
    
    return EventFunnel

# class EventFunnelMethodHolder(object):
#     # Events from NSResponder in 10.6.2
#         
#     # Mouse events
#     def mouseDown_(self, event):
#         print "funnel mouse down"
#         if not handler(self, event):
#             print 'forwarding to', super(self.__class__, self)
#             super(self.__class__, self).mouseDown_(event) # FIXME: need to properly resolve parent class
#         
#     def mouseDragged_(self, event):
#         handler(self, event)
#         
#     def mouseUp_(self, event):
#         handler(self, event)
#         
#     def mouseMoved_(self, event):
#         handler(self, event)
#         
#     def mouseEntered_(self, event):
#         handler(self, event)
#         
#     def mouseExited_(self, event):
#         handler(self, event)
#         
#     def rightMouseDown_(self, event):
#         handler(self, event)
#         
#     def rightMouseDragged_(self, event):
#         handler(self, event)
#         
#     def rightMouseUp_(self, event):
#         handler(self, event)
#         
#     def otherMouseDown_(self, event):
#         handler(self, event)
#         
#     def otherMouseDragged_(self, event):
#         handler(self, event)
#         
#     def otherMouseUp_(self, event):
#         handler(self, event)
#         
#     #Responding to Key Events
#     def keyDown_(self, event):
#         handler(self, event)
#         
#     def keyUp_(self, event):
#         handler(self, event)
#         
#     def interpretKeyEvents_(self, event):
#         handler(self, event)
#         
#     def performKeyEquivalent_(self, event):
#         handler(self, event)
#         
#     def performMnemonic_(self, event):
#         handler(self, event)
#         
#     def flushBufferedKeyEvents_(self, event):
#         handler(self, event)
#         
#         
#     # Responding to Other Kinds of Events
#     def cursorUpdate_(self, event):
#         handler(self, event)
#         
#     def flagsChanged_(self, event):
#         handler(self, event)
#         
#     def tabletPoint_(self, event):
#         handler(self, event)
#         
#     def tabletProximity_(self, event):
#         handler(self, event)
#         
#     def helpRequested_(self, event):
#         handler(self, event)
#         
#     def scrollWheel_(self, event):
#         handler(self, event)

__all__ = 'EventFunnel StateMachineEventFunnel'.split()