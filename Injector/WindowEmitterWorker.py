# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from Foundation import *
from AppKit import *
from Quartz import *
import objc

import hashlib
import random
import select
import socket
import struct
import threading
import time

import jre.cocoa
import jre.debug

from SocketConnection import SocketConnection, SocketConnectionError
from QNDWindowProtocol import QNDWindowProtocol as WindowProtocol
from QNDWindowProtocolTypes import *

_eventMap = { TAP_EVENT_MSG_TYPE: (NSLeftMouseDown, NSLeftMouseUp),
              TOUCH_DOWN_EVENT_MSG_TYPE: (NSLeftMouseDown,),
              TOUCH_MOVED_EVENT_MSG_TYPE: (NSLeftMouseDragged,),
              TOUCH_UP_EVENT_MSG_TYPE: (NSLeftMouseUp,),
            }
                
class ScottyWindowEmitterWorker(SocketConnection):
    def __init__(self, (host, port), delegate=None):
        SocketConnection.__init__(self, (host, port), WindowProtocol(self), self)
        self.delegate = delegate
        self._eventNumber = 0
        
    def socketConnectionClosed(self, connection):
        if self.delegate and hasattr(self.delegate, 'workerConnectionClosed'):
            self.delegate.workerConnectionClosed(self)
    
    def socketConnectionError(self, connection, error):
        if self.delegate and hasattr(self.delegate, 'workerConnectionError'):
            self.delegate.workerConnectionError(self, error)
        else:
            NSLog(u"Lost connection: %s: %s" % (error.__class__.__name__, error))
        self.socketConnectionClosed(connection)
        
    def makeMouseEvent(self, kind, windowID, x, y):
        self._eventNumber += 1
        event = NSEvent.mouseEventWithType_location_modifierFlags_timestamp_windowNumber_context_eventNumber_clickCount_pressure_(
        kind,
        NSMakePoint(x, y),
        0,
        time.time(), #FIXME! should be in time since boot and the time of event, not now.
        windowID,
        NSGraphicsContext.currentContext(),
        self._eventNumber,
        1,
        0.0)
        
        return event
    
    def _handleRemoteMouseEvent(self, eventType, windowID, x, y):
        #NSLog(u"Received %s event at %s,%s" % (hex(eventType), x, y))
        if eventType in _eventMap:
            cocoaEventTypes = _eventMap[eventType]
        else:
            NSLog(u"Unexpected event type: %s" % (hex(eventType)))
            return
        
        x, y = self.translatePoint_toWindowCoordForWindowID_((x, y), windowID)
            
        # Enable grouping of multiple events (e.g. tap/click : down and up) with a single msg
        for cocoaEventType in cocoaEventTypes:
            event = self.makeMouseEvent(cocoaEventType, windowID, x, y)
            NSApp().postEvent_atStart_(event, False)
    
    def translatePoint_toWindowCoordForWindowID_(self, point, windowID):
        result = point
        if self.delegate and hasattr(self.delegate, 'clippingRegionForWindowID_'):
            clippingRegion = self.delegate.clippingRegionForWindowID_(windowID)
            if clippingRegion:
                ((ox, oy), (ow, oh)) = clippingRegion
                wh = NSApp().windowWithWindowNumber_(windowID).frame().size.height # FIXME: might have changed!
                oy = wh - (oh + oy) # Translate origin from upper left to bottom left
                (x, y) = point
                result = (x + ox, y + oy)
        return result
    
    # def sendWindowBuffer_forWindowID_(self, windowBuffer, windowID):
    #     if not isinstance(windowBuffer, NSBitmapImageRep):
    #         bitmap = NSBitmapImageRep.alloc().initWithCGImage_(windowBuffer)
    #     else:
    #         bitmap = windowBuffer
    #     tiffRep = bitmap.TIFFRepresentation()
    # 
    #     self.sendUpdateWindow(windowID, tiffRep)
    #     del bitmap, tiffRep # Should so not be necessary.
    
    @jre.debug.trap_exceptions
    def sendImageData_forWindowID_(self, windowBuffer, windowID):
        clippingRegion = None
        tiffRep = None
        data = None
        if self.delegate and hasattr(self.delegate, 'clippingRegionForWindowID_'):
            clippingRegion = self.delegate.clippingRegionForWindowID_(windowID)
        # Overload the normal sendUpdateWindow to make sure windowBuffer is a TIFF before sending
        if isinstance(windowBuffer, NSBitmapImageRep):
            if clippingRegion:
                windowBuffer = CGImageCreateWithImageInRect(windowBuffer.CGImage(), clippingRegion)
                windowBuffer = NSBitmapImageRep.alloc().initWithCGImage_(windowBuffer)
            data = windowBuffer
            tiffRep = data.TIFFRepresentation()
        elif isinstance(windowBuffer, NSData):
            data = windowBuffer
            # data.writeToFile_atomically_(u"/Users/eaganj/Desktop/scotty-tmp.pdf", True)
        else:
            if clippingRegion:
                windowBuffer = CGImageCreateWithImageInRect(windowBuffer, clippingRegion)
            data = NSBitmapImageRep.alloc().initWithCGImage_(windowBuffer)
            # tiffRep = data.TIFFRepresentation()
            
            tiffRep = data.representationUsingType_properties_(NSPNGFileType, {NSImageCompressionFactor: 8.0})
            # tiffRep.writeToFile_atomically_(u"/Users/eaganj/Desktop/scotty-tmp.png", True)
            # print "Sending %s kB image" % (tiffRep.length() / 1024.0)
        try:
            self.sendUpdateWindow(windowID, tiffRep if tiffRep else data)
        except SocketConnectionError, e:
            # Connection failure; ignore -- such errors are dealt with in the socketConnectionError/Closed
            # methods, but because of concurrence, we might still try to add data when the connection state
            # is invalid.
            #NSLog(u"Could not send window image data: %s" % (e))
            pass
        del data, tiffRep # Should so not be necessary.
    
    
    def touchDownEvent(self, windowID, x, y):
        self._handleRemoteMouseEvent(TOUCH_DOWN_EVENT_MSG_TYPE, windowID, x, y)

    def touchUpEvent(self, windowID, x, y):
        self._handleRemoteMouseEvent(TOUCH_UP_EVENT_MSG_TYPE, windowID, x, y)

    def touchMovedEvent(self, windowID, x, y):
        self._handleRemoteMouseEvent(TOUCH_MOVED_EVENT_MSG_TYPE, windowID, x, y)