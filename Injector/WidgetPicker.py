# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from AppKit import *
from Foundation import *
import objc

import WindowEmitter

import jre.debug

from IIKit import GlassWindow
from DecoratorWindow import DecoratorWindow

from collections import deque
import sys

jre.debug.DEPRECATED()

#class ScottyWidgetPicker(istar.Instrument):
class ScottyWidgetPicker(NSObject):
    def init(self):
        self = super(ScottyWidgetPicker, self).init()
        if not self:
            return self
        
        self._pickerView = None
        self._pickerActiveMode = False
        self._boxActiveMode = False
        self._dirty = False
        
        self.resetSelection()
        
        return self
    
    def resetSelection(self):
        self._window = None
        self._widget = None
    
    def setPickerView_(self, view):
        jre.debug.DEPRECATED()
        self._pickerView = view
    
    def pickerViewForWindow_(self, window):
        sharedSender = WindowEmitter.WindowEmitter.sharedSender()
        return sharedSender.glassPaneForWindow_(window).contentView()
    
    def updateDataForWidget_inWindow_(self, widget, window):
        pickerView = self.pickerViewForWindow_(window)
        pickerView.reset()
        if not self._window:
            return
        
        # Traverse widget tree and add to glass window
        boxAdder = pickerView.addBox_level_ if self._boxActiveMode else None
        self._printTreeRootedAt_withFunc_(window.contentView().superview(), boxAdder)
        
        if widget and self._pickerActiveMode:
            superview = widget.superview()
            widgetFrame = superview.convertRectToBase_(widget.frame()) if superview else widget.convertRectToBase_(widget.frame())
            pickerView.addHighlightBox_(widgetFrame)
            
        if self._dirty or self._pickerActiveMode or self._boxActiveMode:
            #sharedSender.setGlassPaneNeedsDisplayForWindow_(window)
            if pickerView:
                pickerView.setNeedsDisplay_(True)
            self._dirty = False
        
    @jre.debug.trap_exceptions
    def mouseMoved(self, event):
        #jre.debug.TRACE()
        if not self._pickerActiveMode and not self._boxActiveMode and not self._dirty:
            return
        
        #jre.debug.TRACE()
        #NSLog(u"Picker: %s Box: %s Dirty: %s" % (self._pickerActiveMode, self._boxActiveMode, self._dirty))
        
        self.resetSelection()
            
        wlocation = event.locationInWindow() # mouse location in *window* coordinates.
        slocation = event.window().convertBaseToScreen_(wlocation) # mouse location in *screen* coordinates.
        window = self.getWindowAtLocation(slocation) # Is this necessarily the same as the event window?
        self._window = window
        
        if window:
            pickerView = self.pickerViewForWindow_(window)
            pickerView.setMousePoint_(wlocation)
            widget = self.getWindowWidgetAtLocation(window, wlocation)
            self._widget = widget
            
            # if widget:  # Test for Clement
            #     for name in widget.accessibilityAttributeNames():
            #         print u"%s: %s" % (name, widget.accessibilityAttributeValue_(name))
            #     print widget.accessibilityActionNames()
            
            self.updateDataForWidget_inWindow_(widget, window)
    
    def mouseDown(self, event):
        # jre.debug.TRACE()
        jre.debug.DEPRECATED()
        
    def mouseUp(self, event):
        jre.debug.DEPRECATED()
        # jre.debug.TRACE()
        # if self._widget:
        #     widget = self._widget
        #     superview = widget.superview()
        #     sharedSender = WindowEmitter.WindowEmitter.sharedSender()
        #     widgetFrameInWindowCoords = widget.convertRectToWindow_(widget.frame())
        #     sharedSender.addClippingRegion_forWindow_(self._widget.frame(), self._window)
        #     return True
        # 
        # return False
    
    def _printTreeRootedAt_(self, view):
        return self._printTreeRootedAt_level_silent_withFunc_(view, 0, False, None)
    
    def _printTreeRootedAt_withFunc_(self, view, func):
        return self._printTreeRootedAt_level_silent_withFunc_(view, 0, True, func)
        
    def _printTreeRootedAt_level_silent_withFunc_(self, view, level, silent, f):
        subviews = self._printTreeRootedAt_level_silent_withFunc_
        if f:
            f(view, level)
        if not silent:
            #frame = view.convertRectToBase_(view.frame())
            #frame = view.convertRect_toView_(view.frame(), None)
            
            superview = view.superview()
            if superview:
                #frame = view.convertRect_fromView_(view.frame(), superview)
                frame = superview.convertRectToBase_(view.frame())
            else:
                frame = view.convertRectToBase_(view.frame())
                
            print '  '*level, view, #view.__class__.__name__,
            #print "((%s, %s), (%s x %s))" % (frame.origin.x, frame.origin.y, 
            #                                 frame.size.width, frame.size.height)
            print "x:[%s, %s], y:[%s, %s]" % (frame.origin.x, frame.origin.x + frame.size.width,
                                              frame.origin.y, frame.origin.y + frame.size.height)
        return [ view.__class__.__name__, ] + [ subviews(subview, level+1, silent, f) \
                                                                for subview in view.subviews() ]
    
    def getWindowAtLocation(self, location):
        for window in NSApp().orderedWindows():
            if isinstance(window, (GlassWindow, DecoratorWindow)) or not window.isVisible():
                continue
            if NSMouseInRect(location, window.frame(), False):
                return window
        return None
        
    def getWindowWidgetAtLocation(self, window, location):
        result = None
        cocoaPrivateFrameView = window.contentView().superview()
        view = cocoaPrivateFrameView
        queue = deque([ view ])
        while queue:
            view = queue.pop()
            #if view.frame().origin.x < 0 or view.frame().origin.y < 0:
            #    continue
                
            superview = view.superview()
            if superview:
                frame = superview.convertRectToBase_(view.frame())
            else:
                frame = view.convertRectToBase_(view.frame())
                
            if NSMouseInRect(location, frame, False):
               result = view
               subviews = view.subviews()
               if not subviews:
                   break
               queue.extendleft(view.subviews())
        
        #return result if result != cocoaPrivateFrameView else None # Only include contentView and below
        return result
    
    def setNeedsDisplay(self):
        self.updateDataForWidget_inWindow_(self._widget, self._window)
    
    def setPickerActive_forWindow_(self, active, window):
        self._pickerActiveMode = active
        self._window = window
        if not active and self._window:
            sharedSender = WindowEmitter.WindowEmitter.sharedSender()
            sharedSender.clearClippingRegionForWindow_(self._window)
            
        # for window in NSApp().orderedWindows():
        #     if not window.isVisible():
        #         continue
        #     print u'%s: %s "%s" <-' % (window.windowNumber(), window, window.title()),
        #     print window.parentWindow().windowNumber() if window.parentWindow() else u''
        # print '--'
            
        if self._window:
            self.pickerViewForWindow_(self._window).setInteractive_(active)
        self._dirty = True
        self.setNeedsDisplay()
    
    def pickerActive(self):
        return self._pickerActiveMode
        
    def setWireframeActive_forWindow_(self, active, window):
        self._boxActiveMode = active
        self._window = window
        self._dirty = True
        self.setNeedsDisplay()
    
    def wireframeActive(self):
        return self._boxActiveMode
    
    def takePickerActiveMode_(self, sender):
        # self._window = sender.window().parentWindow()
        # sender.window().orderFront_(sender)
        self.setPickerActive_forWindow_(sender.state() == NSOnState, sender.window().parentWindow())

    def takeWireframeActiveMode_(self, sender):
        # self._window = sender.window().parentWindow()
        # sender.window().orderFront_(sender)
        self.setWireframeActive_forWindow_(sender.state() == NSOnState, sender.window().parentWindow())

        
WidgetPicker = ScottyWidgetPicker



# class ScottyWidgetPickerProtocol(istar.Protocol):
#     @classmethod
#     def objectConforms_(cls, obj):
#         return isinstance(obj, NSView)
#     
#     @classmethod
#     def bind_(cls, obj):
#         proto = super(ScottyWidgetPickerAction, cls).bind_(obj)
#         
#         # Do stuff here
#         
#         return proto
# 
# WidgetPickerProtocol = ScottyWidgetPickerProtocol



# class ScottyWidgetPickerAction(istar.Action):
#     pass
# 
# WidgetPickerAction = ScottyWidgetPickerAction