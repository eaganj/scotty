# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from AppKit import *
from Foundation import *
import objc

import WindowEmitter # FIXME FIXME FIXME

import jre.debug

from WidgetPickerDecorator import *

from collections import deque

class ScottyDecoratorWindowView(NSView):
    def __new__(cls, frame):
        return cls.alloc().initWithFrame_(frame)
    
    def initWithFrame_(self, frame):
        self = super(ScottyDecoratorWindowView, self).initWithFrame_(frame)
        if not self:
            return self
        
        self._decorators = []
        self._shouldRedrawShadow = True
        
        sharedSender = WindowEmitter.WindowEmitter.sharedSender()
        pickerDecorator = WidgetPickerDecorator()
        pickerDecorator.setWidgetPickerType_(WidgetPickerLeafViewType)
        pickerDecorator.setTarget_(sharedSender._widgetPicker)
        pickerDecorator.setAction_('takePickerActiveMode:')
        pickerDecorator.setToolTip_(u"Highlight deepest widget.")
        boxDecorator = WidgetPickerDecorator()
        boxDecorator.setWidgetPickerType_(WidgetPickerBoxType)
        boxDecorator.setTarget_(sharedSender._widgetPicker)
        boxDecorator.setAction_('takeWireframeActiveMode:')
        boxDecorator.setToolTip_(u"Show view hierarchy.")
        pickerDecoratorFrame = pickerDecorator.frame()
        boxDecorator.setFrameOrigin_(NSMakePoint(
                                     pickerDecoratorFrame.origin.x,
                                     pickerDecoratorFrame.origin.y + pickerDecoratorFrame.size.height + 4))
        self.addDecorator_(pickerDecorator)
        self.addDecorator_(boxDecorator)
        
        return self
        
    def addDecorator_(self, decorator):
        self.addSubview_(decorator)
        self._decorators.append(decorator)
        decorator.setFrameOrigin_((decorator.frame().origin.x + 2, decorator.frame().origin.y))
        self._shouldRedrawShadow = True
        
    def drawRect_(self, rect):
        NSColor.clearColor().set()
        NSRectFill(self.frame())
        
        path = NSBezierPath.bezierPath()
        rect = self.frame()
        rect.size.width -= 2
        radius = 5
        inset = NSInsetRect(rect, radius, radius)
        
        path.moveToPoint_((NSMinX(rect), NSMinY(rect)))
        path.appendBezierPathWithArcWithCenter_radius_startAngle_endAngle_((NSMaxX(inset), NSMinY(inset)), radius, 270, 360)
        path.appendBezierPathWithArcWithCenter_radius_startAngle_endAngle_((NSMaxX(inset), NSMaxY(inset)), radius, 0, 90)
        path.lineToPoint_((NSMinX(rect), NSMaxY(rect)))
        path.closePath()
        NSColor.windowFrameColor().set()
        path.fill()
        
        if self._shouldRedrawShadow:
            # Force window to update its shadow in case our shape has changed.
            window = self.window()
            if window.hasShadow():
                window.invalidateShadow()
            
            self._shouldRedrawShadow = False
    
    def mouseDown_(self, event):
        print 'mouse down 1'
    
DecoratorWindowView = ScottyDecoratorWindowView