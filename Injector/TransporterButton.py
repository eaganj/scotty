# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

import jre.cocoa
import jre.debug

from WindowEmitter import WindowEmitter
from ScottyWindowButton import ScottyWindowButton

BUTTON_SIZE = 15, 15
BUTTON_ORIGIN = 70, 4

ModuleBundle = objc.currentBundle()

def loadImage_(path):
    return NSImage.alloc().initWithContentsOfFile_(ModuleBundle.pathForImageResource_(path))
    
class ScottyTransporterButton(ScottyWindowButton):
    #targetWindow = objc.ivar()
    focusedImage = loadImage_(u"greybutton-trans.png")
    unfocusedImage = loadImage_(u"greybutton-disabled-trans")
    focusedActiveImage = loadImage_(u"greybutton-active-trans")
    unfocusedActiveImage = loadImage_(u"greybutton-disabled-active-trans")
    
    imageMap = [ [ unfocusedImage, unfocusedActiveImage ],
                 [ focusedImage, focusedActiveImage ] ]
    
    def initWithTargetWindow_(self, window):
        ((winX, winY), (winW, winH)) = window.frame()
        buttonFrame = NSMakeRect(BUTTON_ORIGIN[0], winH - BUTTON_ORIGIN[1] - BUTTON_SIZE[1],
                                 BUTTON_SIZE[0], BUTTON_SIZE[1])
        self = super(ScottyTransporterButton, self).initWithFrame_(buttonFrame)
        if not self:
            return self
        
        self.targetWindow = window
        
        self.setButtonType_(NSMomentaryChangeButton)
        self.setTitle_(u"")
        self.setBordered_(False)
        self.updateImage()
        self.setTarget_(self)
        self.setAction_(self.toggleTransporterButton_)
        
        return self
    
    def toggleTransporterButton_(self, sender):
        emitter = WindowEmitter.sharedSender()
        emitter.enableHook_forWindow_(self.state() == NSOnState, self.targetWindow)
        self.updateImage()
    
    def updateImage(self):
        image = self.imageMap[ self.targetWindow.isMainWindow() ][ self.state() == NSOnState ]
        self.setImage_(image)
    
    def isOpaque(self):
        return False

TransporterButton = ScottyTransporterButton