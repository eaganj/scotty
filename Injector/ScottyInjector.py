# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

'''
The Scotty Injector actually consists of two primary entry points.  ScottyInjector (this module) injects
its plugin into the currently-running instance of the target application.  Once loaded into the target
applications program space, the plugin integrates itself as necessary to relay updates to the Scotty Viewer.
'''

from Foundation import *
from AppKit import *
import objc

import os
import time


APP = u'com.apple.Dictionary'
APP = u'org.videolan.vlc'

def main(argv=None):
    if argv is None:
        import sys
        argv = sys.argv
    
    NSLog(u"argv : %s" % (argv))
    
    ws = NSWorkspace.sharedWorkspace()
    running = dict([ (app['NSApplicationBundleIdentifier'], app['NSApplicationProcessIdentifier']) for app in ws.launchedApplications() ])
    if APP in running:
        #NSLog(u"Should inject (%s/%s)" % (os.getuid(), os.geteuid()))
        NSLog(u"Injection : %s %s" % (running[APP], getPluginPath()))
        objc.inject(running[APP], getPluginPath())
        time.sleep(1.5)


def getPluginPath():
    pluginPath = NSBundle.mainBundle().pathForResource_ofType_(u'ScottyPlugin', u'plugin')
    exePath = os.path.join(pluginPath, u'Contents', u'MacOS', u'ScottyPlugin')
    return exePath

if __name__ == u'__main__':
    main()