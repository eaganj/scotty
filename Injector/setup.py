# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
from plistlib import Plist
import py2app

import glob

plist = Plist.fromFile('Info.plist')
                  
py2app_options = dict(
        plist=plist,
    )


alias_build_files = glob.glob('*.pth')
plugin = setup(
            plugin=['ScottyPlugin.py'],
            data_files=glob.glob('Plugin NIBs/*[!~].nib') + glob.glob('Plugin Resources/*[!~]') + alias_build_files\
                       [
                            # Add stuff here
                            u'../../../jre',
                            u'../../iStar/istar',
                       ],
         )
scotty_plugin_dir = plugin.plugin[0].appdir

setup(
        app=['ScottyInjector.py'],
        data_files=glob.glob('NIBs/*[!~].nib') + glob.glob('Resources/*[!~]') +\
                   [
                        # Add stuff here
                        scotty_plugin_dir,
                        u'../../../jre',
                   ],
        options=dict(
                      py2app=py2app_options
                    )
    )
