# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

import jre.debug
import jre.cocoa

@jre.debug.trap_exceptions
def module_init():
    import WindowHook
    import PrintPanelHooks
    import jre.debug
    #import istar
    


    class ScottyPlugin(NSObject):
        def startPlugin_(self, sender):
            WindowHook.addWindowHook()
            PrintPanelHooks.addPrintPanelHooks()

    pool = NSAutoreleasePool.alloc().init()

    plugin = ScottyPlugin.alloc().init()
    plugin.performSelectorOnMainThread_withObject_waitUntilDone_(plugin.startPlugin_, None, True)

    del pool

### This really should not be necessary.  For some reason the external bootstrap process is loading
### this module twice, but only some of the time.  Furthermore, Python is executing the module
### each time, instead of only the first time!  This is an ugly variant of the C #ifdef __MODULE_H
### monstrosity.  But it eliminates an ugly Objective-C stack-dump.
global _loadedOnce
try:
    _loadedOnce
except:
    _loadedOnce = True
    module_init()
else:
    NSLog(u"Ignoring second ScottyPlugin load attempt.")