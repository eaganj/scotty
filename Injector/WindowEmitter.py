# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

from Quartz import *

import copy
import hashlib
import repr
import socket
import struct
import time

import jre.cocoa
import jre.debug


from WindowEmitterWorker import ScottyWindowEmitterWorker
from ScottyController import Scotty

class ScottyWindowEmitter(NSObject):
    _singleton = None
    _registry = objc.ivar()
    
    @classmethod
    @jre.debug.trap_exceptions
    def sharedSender(cls):
        if cls._singleton == None:
            cls._singleton = ScottyWindowEmitter.alloc().init()
        
        return cls._singleton
    
    def init(self):
        self = super(ScottyWindowEmitter, self).init()
        if not self:
            return self
        
        self._worker = None
        
        self._registry = set()
        self._pdfRegistry = set()
        self._windowButtons = {}
        self._clippingRegions = {}
        
        self._windowOrderHackList = []
        self._glassPanes = {}
        
        return self
    
    def _verifyWorker(self):
        if self._worker is None or not self._worker.isAlive():
            host, port = Scotty().prefs['serverHost'], Scotty().prefs['serverPort']
            self._worker = ScottyWindowEmitterWorker((host, port), self)
            self._worker.start()
    
    def workerConnectionClosed(self, worker):
        self._worker = None
        windows = [ window for window in self._registry ]
        for window in windows:
            try:
                button = self.buttonForWindow_(window)
                button.setState_(NSOffState)
                button.updateImage()
            except KeyError:
                pass # Expected for windows without transporter buttons.
                
            self._registry.remove(window)
            
    
    def workerConnectionError(self, worker, error):
        jre.cocoa.util.showAlertForWindow(None, 
                                          u'Scotty Server Disconnected',
                                          u'%s' % (error))
        self.workerConnectionClosed(worker)
        
        
    def sendImageDataForWindow_(self, window):
        backing = self.getWindowBacking_(window)
        
        windowID = window.windowNumber()
        startAddTime = time.time()
        # TODO : verify that worker is alive and respawn if necessary.
        #self._worker.sendWindowBuffer_forWindowID_(backing, windowID)
        self._doSendImageDataForWindow_withBacking_(windowID, backing)
        #self._worker.sendUpdateWindow(windowID, backing)
        del backing
        return startAddTime # For profiling purposes
    
    def sendImageDataForWindowLater_(self, window):
        backing = self.getWindowBackingViaPDF_(window)
        # print repr.repr(backing)
        # print "--> Made backing (%s B)" % (len(backing)), NSImage.alloc().initWithData_(backing) != None, hashlib.md5(backing).hexdigest()
        self._doSendImageDataForWindow_withBacking_(window.windowNumber(), backing)
        del backing
    
    def _doSendImageDataForWindow_withBacking_(self, windowID, backing):
        self._verifyWorker()
        #self._worker.sendUpdateWindow(windowID, backing)
        #clip = self._clippingRegions.get(windowID, None)
        self._worker.sendImageData_forWindowID_(backing, windowID)
    
    def getWindowBackingViaCG_(self, window): # FIXME: Need to composite child windows, too.
        ''' Fetches the window backing directly from the Core Graphics public APIs.  This is probably
            the more legitimate way of grabbing the window pixels, but it is not completely clean.  It
            does involve grabbing the window contents from the window server rather than the actual
            backing buffer.  Furthermore, the use of window.windowNumber() in this way is actually 
            an undocumented use of the windowNumber.  Here we rely on the implementation detail that
            (in Leopard, at least) this number is also the CGImageRef.
        '''
        windowID = window.windowNumber()
        
        # Window list options :
        # - kCGWindowListOptionAll
        # - kCGWindowListOptionOnScreenOnly
        # - kCGWindowListOptionOnScreenAboveWindow
        # - kCGWindowListOptionOnScreenBelowWindow
        # - kCGWindowListOptionIncludingWindow
        # - kCGWindowListExcludeDesktopElements
        windowListOptions = kCGWindowListOptionIncludingWindow
        
        # Window image options :
        # - kCGWindowImageDefault - include window and shadow
        # - kCGWindowImageBoundsIgnoreFraming - include window only
        # - kCGWindowImageShouldBeOpaque - make backgroup white not transparent
        # - kCGWindowImageOnlyShadows
        windowImageOptions = kCGWindowImageBoundsIgnoreFraming
        backing = CGWindowListCreateImage(CGRectNull, 
                                          windowListOptions, 
                                          windowID, 
                                          windowImageOptions)
        return backing
    
    def getWindowBackingViaSuperview_(self, window):
        ''' Fetches the window backing by accessing the undocumented superview of the window's 
            contentView.  The existence of this superview and its use in this way is entirely undocumented
            and potentially relies on an implementation detail.  Nonetheless, this approach feels in
            many ways to be cleaner than the Core Graphics approach above, but performs much slower.
        '''
        # Access the window's proper view via the contentView
        contentView = window.contentView()
        view = contentView and contentView.superview()
        if not contentView or not view:
            # Fallback on the Core Graphics method
            NSLog(u"Fallback on CG method")
            return self.getWindowBackingViaCG_(window)
        
        # Grab the pixel backing
        # FIXME: this could be optimized by guarding the bitmap as long as the frame does not change
        bitmap = view.bitmapImageRepForCachingDisplayInRect_(view.frame())
        view.cacheDisplayInRect_toBitmapImageRep_(view.frame(), bitmap)
        
        return bitmap
    
    def getWindowBackingViaSuperview2_(self, window):
        contentView = window.contentView()
        view = contentView and contentView.superview()
        if not contentView or not view:
            #Fallback
            return self.getWindowBackingViaCG_(window)
        
        try:
            view.lockFocus()
            bitmap = NSBitmapImageRep.alloc().initWithFocusedViewRect_(view.bounds())
        finally:
            view.unlockFocus()
        
        return bitmap
    
    def getWindowBackingViaPDF_(self, window):
        ''' Warning: this method must not be called during standard rendering (e.g. in flushWindow) '''
        contentView = window.contentView()
        view = contentView and contentView.superview()
        if not contentView or not view:
            # Fallback
            return self.getWindowBackingViaCG_(window)
        
        data = window.dataWithPDFInsideRect_(NSMakeRect(0, 0, *window.frame().size))
        
        return data
        
    
    def runTimingTest(self, window):
        # In timing tests on Dictionary.app, it appears that the CG method saves 4-6 ms versus the 
        # superview method.  In Safari.app, CG saves 40-80 ms.  Tests performed on my 2 GHz Core Duo.
        startCGTime = time.time()
        #backing = self.getWindowBackingViaCG_(window)
        backing = self.getWindowBackingViaSuperview_(window)
        endCGTime = time.time()
        
        startSuperviewTime = time.time()
        backing = self.getWindowBackingViaSuperview2_(window)
        endSuperviewTime = time.time()
        
        dCG = endCGTime - startCGTime
        dSV = endSuperviewTime - startSuperviewTime
        # fastest, method, dt = min((dCG, u'Core Graphics', dSV-dCG),
        #                           (dSV, u'Superview', dCG-dSV))
        fastest, method, dt = min((dCG, u'Superview', dSV-dCG),
                                  (dSV, u'Superview2', dCG-dSV))
        NSLog(u"%s method saves %s s (took %s s)." % (method, dt, fastest))
        
        return backing
    
    getWindowBacking_ = getWindowBackingViaCG_ # Use Core Graphics backing method
    #getWindowBacking_ = getWindowBackingViaSuperview_ # Use hackish backing method
    #getWindowBacking_ = getWindowBackingViaSuperview2_ # Use hackish backing method2 (Still too slow)
    #getWindowBacking_ = getWindowBackingViaPDF_ # Get vector-based backing (buggy and slow!)
    #getWindowBacking_ = runTimingTest
    
    def enableStandardHook_forWindow_(self, state, window):
        # FIXME : this registry will hold a reference to the window, keeping it from ever being released.
        #         We need to make sure we hook into window destroy events, too.
        if state:
            self._registry.add(window)
        else:
            try:
                self._registry.remove(window)
                # Window successfully removed, which means it was already being transported.  Inform
                # the server that the window has been unmapped.  
                # TODO: We still should detect window destroy events and send those, as well.
                self._verifyWorker()
                self._worker.sendHideWindow(window.windowNumber())
            except KeyError:
                pass # ignore if not already registered
        
        windowButton = self.buttonForWindow_(window)
        if windowButton:
            # FIXME: this should be refactored to use an observer pattern
            windowButton.setState_(state)
    
    def enablePDFHook_forWindow_(self, state, window):
        if state:
            self._pdfRegistry.add(window)
        else:
            try:
                self._pdfRegistry.remove(window)
                self._verifyWorker()
                self._worker.sendHideWindow(window.windowNumber())
            except KeyError:
                pass
        
        windowButton = self.buttonForWindow_(window)
        if windowButton:
            windowButton.setState_(state)
    
    def enableHook_forWindow_(self, state, window):
        if Scotty().prefs['debugSendWindowsAsPDF']:
            self.enablePDFHook_forWindow_(state, window)
        else:
            self.enableStandardHook_forWindow_(state, window)
    
    def hookEnabledForWindow_(self, window):
        if Scotty().prefs['debugSendWindowsAsPDF']:
            return self.pdfHookEnabledForWindow_(window)
        else:
            return self.standardHookEnabledForWindow_(window)
            
    def standardHookEnabledForWindow_(self, window):
        return window in self._registry
    
    def pdfHookEnabledForWindow_(self, window):
        return window in self._pdfRegistry
    
    def buttonForWindow_(self, window):
        return self._windowButtons[window]
    
    def addButton_forWindow_(self, button, window):
        self._windowButtons[window] = button
    
    def addClippingRegion_forWindow_(self, region, window):
        ((x, y), (w, h)) = region
        # print 'Clipping', window.title(), 'to', '(%s, %s), (%s x %s)' % (x, y, w, h)
        self._clippingRegions[window.windowNumber()] = region
    
    def clearClippingRegionForWindow_(self, window):
        if window in self._clippingRegions:
            del self._clippingRegions[window.windowNumber()]
    
    def clippingRegionForWindowID_(self, windowID):
        return self._clippingRegions.get(windowID, None)
    
    def printNotification_(self, notification):
        NSLog(u"Received notification: %s" % notification)
    
    def getWindowOrderViaUglyHack(self):
        self.resetWindowOrderHack()
        NSApp().makeWindowsPerform_inOrder_('scottyWindowOrderHack', True)
        result = self._windowOrderHackList
        self.resetWindowOrderHack()
        return result
        
    def recordWindowOrderHack_(self, window):
        self._windowOrderHackList.append(window.windowNumber())
    
    def resetWindowOrderHack(self):
        self._windowOrderHackList = []
    
    def addGlassPane_forWindow_(self, pane, window):
        self._glassPanes[window] = pane
    
    def glassPaneForWindow_(self, window):
        return self._glassPanes.get(window, None)
        
    def updateGlassPaneForWindow_(self, window):
        pass
        # pane = self._glassPanes.get(window, None)
        # if not pane:
        #     return
        # try:
        #     pane.setFrameOrigin_(window.frame().origin)
        # except:
        #     print "Error in updateGlassPaneForWindow_", window, pane
        #     raise
    
    def setGlassPaneNeedsDisplayForWindow_(self, window):
        pane = self._glassPanes.get(window, None)
        if not pane:
            return
        
        pane.contentView().setNeedsDisplay_(True)
    
WindowEmitter = ScottyWindowEmitter

__all__ = ['WindowEmitter', 'ScottyWindowEmitter']