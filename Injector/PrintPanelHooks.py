# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

import time # For profiling

from ScottyController import *
from WindowEmitter import WindowEmitter

import jre.cocoa
import jre.debug

def addPrintPanelHooks():
    # - beginSheetWithPrintInfo:modalForWindow:delegate:didEndSelector:contextInfo:  
    # - runModal  
    # - runModalWithPrintInfo:
    
    @jre.cocoa.swizzleMethod(
            NSPrintPanel.beginSheetWithPrintInfo_modalForWindow_delegate_didEndSelector_contextInfo_)
    def scottyBeginSheetWithPrintInfo_modalForWindow_delegate_didEndSelector_contextInfo_(
            self, info, window, delegate, selector, context):
        try:
            # Call the swizzled out original version
            self.scottyBeginSheetWithPrintInfo_modalForWindow_delegate_didEndSelector_contextInfo_(
                            info, window, delegate, selector, context)

            # `window` is the wrong window !  This is actually the parent window of the sheet.
            sheet = window.attachedSheet()
            if sheet and Scotty().prefs['automaticallyTeleportPrinterDialogs']:
                WindowEmitter.sharedSender().enableHook_forWindow_(True, sheet)
        except:
            jre.debug.printStackTrace(u"Error in swizzled flushWindow()")