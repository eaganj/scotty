# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from AppKit import *
from Foundation import *
import objc

import jre.cocoa
import jre.debug

# This method needs to be above the following imports to deal with reference cycles.  The ScottyController
# can be imported as "from ScottyController import Scotty" to help work avoid such problems. -JRE
def Scotty():
    return ScottyController.sharedController()
    
import WindowEmitter
import ScottyPreferencesWindowController
from ScottyPreferencesWindowController import PreferencesWindowController
#from InstrumentManager import *

ModuleBundle = objc.currentBundle()

class ScottyController(jre.cocoa.Singleton):
    _prefsWindowController = objc.ivar()
    
    #sharedController = jre.cocoa.Singleton.sharedSingleton
    @classmethod
    def sharedController(cls):
        return cls.sharedSingleton()
    
    def init(self):
        self = super(ScottyController, self).init()
        if not self:
            return self
        
        bundle = ModuleBundle #objc.currentBundle()
        bundleID = bundle.bundleIdentifier()
        self.prefs = jre.cocoa.BundleUserDefaults(bundleID)
        defaultDefaults = bundle.pathForResource_ofType_(u"Defaults", u"plist")
        defaultDefaults = NSDictionary.dictionaryWithContentsOfFile_(defaultDefaults)
        self.prefs.registerDefaults_(defaultDefaults)
        
        self._prefsWindowController = None
        
        from IIKit import InstrumentManager # We import here to avoid an import cycle.  
        # Since the ScottyController is a singleton, this code should only happen once, so we can
        # accept the late import.
        self._instrumentManager = InstrumentManager.sharedInstrumentManager()
        self._glassWindows = {}
        
        
        return self
    
    def sharedSender(self):
        return WindowEmitter.WindowEmitter.sharedSender()
    
    def showPreferences_(self, sender):
        if not self._prefsWindowController:
            self._prefsWindowController = PreferencesWindowController(self.prefs)
        self._prefsWindowController.showWindow_(sender)
    
    def registerGlassWindow_(self, glassWindow):
        self._glassWindows[glassWindow.parentWindow()] = glassWindow
    
    def unregisterGlassWindow_(self, glassWindow):
        del self._glassWindows[glassWindow.parentWindow()]
    
    def glassWindowForWindow_(self, window):
        return self._glassWindows.get(window, None)
        
    def grabGlassWindowsForInstrument_hijackingInteraction_(self, instrument, hijack):
        jre.debug.DEPRECATED()
        glassViews = []
        for parentWindow, glassWindow in self._glassWindows.items():
            print "grab glass window with parent", parentWindow
            if parentWindow:
                parentWindow.addChildWindow_ordered_(glassWindow, NSWindowAbove)
            else:
                glassWindow.orderWindow_relativeTo_(NSWindowAbove, 0)
                
            glassWindow.setHijacksInteraction_(hijack)
            glassView = instrument.newGlassViewForGlassWindow(glassWindow)
            glassViews.append(glassView)
            # glassView = glassView if glassView else NSView.alloc().init()
            # glassWindow.setContentView_(glassView)
            if hijack:
                # Hook into the responder chain
                keyWindow = NSApp().keyWindow()
                if keyWindow:
                    if keyWindow.firstResponder():
                        nextResponder = keyWindow.firstResponder()
                    else:
                        nextResponder = keyWindow
                    glassWindow.setNextResponder_(nextResponder)
                
                # Take key focus
                glassWindow.makeKeyWindow()
        
        return glassViews
    
    # def attachToActivationSource_(self, context):
    #     # TODO: Refactor into context: context.attachGlassWindowToActivationSource()
    #     GlassWindow.attachToActivationSource_(context)
    
    # def grabFullScreenGlassWindowForInstrument_hijackingInteraction_(self, instrument, hijack):
    #     glassWindow = GlassWindow.alloc().initFullScreen()
    
    def ungrabGlassWindowsForInstrument_(self, instrument):
        jre.debug.DEPRECATED()
        for glassWindow in self._glassWindows.values():
            glassWindow.setHijacksInteraction_(False)
            glassWindow.setContentView_(NSView.alloc().init())
            glassWindow.orderOut_(None)
    
    def activateInstrument_withContext_(self, instrumentID, context):
        instrumentManager = self._instrumentManager
        instrument = instrumentManager.instrumentWithID_(instrumentID)
        instrumentManager.activateInstrument_withContext_once_(instrument, context, True)
    
    def applicationMouseMoved_(self, event):
        #self._instrumentManager.mouseMoved_(event)
        return self._instrumentManager.handleEvent(event, 'fr.lri.insitu.Scotty')
    
    def handleEvent_(self, event):
        return self._instrumentManager.handleEvent(event, 'fr.lri.insitu.Scotty')

__all__ = ['ScottyController', 'Scotty']