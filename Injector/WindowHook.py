# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc
objc.setVerbose(1)

from Quartz import *

import time # For profiling

from WindowEmitter import WindowEmitter
from TransporterButton import TransporterButton
from ScottyWindowButton import WindowButton
#from WidgetPicker import GlassWindow, DecoratorWindow
#from IIKit import GlassWindow
#from DecoratorWindow import DecoratorWindow
from IIKit.scotty import *
from ScottyController import *
import jre.cocoa
import jre.debug

DefaultNotificationCenter = NSNotificationCenter.defaultCenter()

def addWindowHook():
    swizzleWindowMethods()
    addUIHook()


skipNextFlushWindow = 0
def swizzleWindowMethods():
    @jre.cocoa.swizzleMethod(NSWindow.flushWindow)
    def myFlushWindow(self):
        try:
            global skipNextFlushWindow
            start = time.time()
            DefaultNotificationCenter.postNotificationName_object_(u"ScottyWindowIsUpdating", self)
            self.myFlushWindow() # This will call the swizzled out original version
            middle = time.time()
            DefaultNotificationCenter.postNotificationName_object_(u"ScottyWindowDidUpdate", self)
            sharedSender = WindowEmitter.sharedSender()
            sharedSender.updateGlassPaneForWindow_(self)
            hookButton = [ view for view in self.contentView().superview().subviews() 
                                                    if isinstance(view, WindowButton) ]
            if hookButton:
                hookButton[0].updateImage()
            if sharedSender.hookEnabledForWindow_(self):
                #if not NSApp().windows() or self != NSApp().windows()[0]:
                #    return
                
                if sharedSender.pdfHookEnabledForWindow_(self):
                    qstart = time.time()
                    if not skipNextFlushWindow:
                        sharedSender.performSelectorOnMainThread_withObject_waitUntilDone_(
                                            sharedSender.sendImageDataForWindowLater_,
                                            self,
                                            False)
                        skipNextFlushWindow += 2 # Magic; apparently the "getPDFbacking" method flushes twice
                    else:
                        skipNextFlushWindow -= 1
                else:
                    qstart = sharedSender.sendImageDataForWindow_(self)
                
                end = time.time()
                if end-start > 0.25:
                    NSLog(u"Took %5s extra seconds (%3sq) for %5s total" % (end-middle, end-qstart, end-start))
        except Exception, e:
            jre.debug.printStackTrace(u"Error in swizzled flushWindow()")

    @jre.cocoa.swizzleMethod(NSWindow.initWithContentRect_styleMask_backing_defer_)
    def scottyInitWithContentRect_styleMask_backing_defer_(self, rect, mask, backing, defer):
        self = self.scottyInitWithContentRect_styleMask_backing_defer_(rect, mask, backing, defer)
        if not self:
            return self
        
        try:
            DefaultNotificationCenter.postNotificationName_object_("ScottyWindowCreated", self)
            addUIHookForWindow_(self)
        except Exception, e:
            jre.debug.printStackTrace(u"Error in swizzled method")

        return self
    
    # @jre.cocoa.swizzleMethod(NSWindow.initWithContentRect_styleMask_backing_defer_screen_)
    # def scottyInitWithContentRect_styleMask_backing_defer_screen_(self, rect, mask, backing, defer, screen):
    #     self = scottyInitWithContentRect_styleMask_backing_defer_screen_(self, rect, mask, backing, 
    #                                                                      defer, screen)
    #     if not self:
    #         return self
    #     
    #     try:
    #         addUIHookForWindow_(self)
    #     except Exception, e:
    #         jre.debug.printStackTrace(u"Error in swizzled method")
    #         
    #     return self
    
    @jre.cocoa.swizzleMethod(NSWindow.orderWindow_relativeTo_)
    def scottyOrderWindow_relativeTo_(self, order, windowID):
        # print "ordered window", self, order, "relative to", windowID
        # addGlassPaneForWindow_(self)
        self.scottyOrderWindow_relativeTo_(order, windowID)
        children = self.childWindows() or []
        for child in children:
            child.scottyParentWindowWasReordered_relativeTo_(order, windowID)
    
    @jre.cocoa.swizzleMethod(NSWindow.orderOut_)
    def scottyOrderOut_(self, sender):
        DefaultNotificationCenter.postNotificationName_object_(u'ScottyWindowWillHide', self)
        self.scottyOrderOut_(sender)
    
    # @jre.cocoa.swizzleMethod(NSApplication.mouseMoved_)
    # def scottyMouseMoved_(self, event):
    #     # NOTE: NSApplication does not actually implement mouseMoved:; it is inherited from
    #     #       NSResponder, so this actually swizzles the NSResponder version!
    #     #print '->', event.timestamp(), self
    #     # if not isinstance(self, NSApplication) or not Scotty().applicationMouseMoved_(event):
    #     if not Scotty().applicationMouseMoved_(event):
    #         # Allow Scotty to intercept mouse events by returning True
    #         #jre.debug.TRACE()
    #         return self.scottyMouseMoved_(event)


    # # testing testing testing
    # bundleID = objc.currentBundle().bundleIdentifier()
    # userDefaults = jre.cocoa.BundleUserDefaults.alloc().initWithPersistentDomainName_(bundleID)
    # userDefaults.setBool_forKey_(False, 'ScottyShouldLoad')
    # userDefaults.synchronize()
    
    # import ScottyPreferencesWindowController
    # global prefsController
    # prefsController = ScottyPreferencesWindowController.ScottyPreferencesWindowController()
    # prefsController.showWindow_(None)
    
class NSWindow(objc.Category(NSWindow)):
    def scottyWindowOrderHack(self):
        sharedSender = WindowEmitter.sharedSender()
        sharedSender.recordWindowOrderHack_(self)
    
    def scottyParentWindowWasReordered_relativeTo_(self, order, windowID):
        pass

class NSApplication(objc.Category(NSApplication)):
    def allOrderedWindows(self):
        # Unlike `NSApplication.orderedWindows()`, which only returns scriptable windows, this message queries
        # the window server to retrieve a list of ALL windows for the application, in stacking order.  
        # Warning: This method uses `NSApplication.contextID()`, which is undocumented, private API.  
        contextID = self.contextID()
        count = NSCountWindowsForContext(contextID, None)
        return map(self.windowWithWindowNumber_, NSWindowListForContext(contextID, count, None))

def addUIHook():
    for window in NSApp().windows():
        addUIHookForWindow_(window)

def addUIHookForWindow_(window):
    # addGlassPaneForWindow_(window)
    
    if not (window.styleMask() & (NSClosableWindowMask|NSMiniaturizableWindowMask|NSResizableWindowMask)):
        # Skip windows without a title bar
        # mask = window.styleMask()
        # availableOptions = { NSBorderlessWindowMask: u"NSBorderlessWindowMask", 
        #                      NSTitledWindowMask: u"NSTitledWindowMask",
        #                      NSClosableWindowMask: u"NSClosableWindowMask",
        #                      NSMiniaturizableWindowMask: u"NSMiniaturizableWindowMask",
        #                      NSResizableWindowMask: u"NSResizableWindowMask",
        #                      NSTexturedBackgroundWindowMask: u"NSTexturedBackgroundWindowMask",
        #                    }
        # options = [ availableOptions[option] for option in availableOptions if mask & option ]
        # NSLog(u"Ignoring window %s with options: %s" % (window.title(), u'|'.join(options)))
        return
        
    # mask = window.styleMask()
    # availableOptions = { NSBorderlessWindowMask: u"NSBorderlessWindowMask", 
    #                      NSTitledWindowMask: u"NSTitledWindowMask",
    #                      NSClosableWindowMask: u"NSClosableWindowMask",
    #                      NSMiniaturizableWindowMask: u"NSMiniaturizableWindowMask",
    #                      NSResizableWindowMask: u"NSResizableWindowMask",
    #                      NSTexturedBackgroundWindowMask: u"NSTexturedBackgroundWindowMask",
    #                    }
    # options = [ availableOptions[option] for option in availableOptions if mask & option ]
    # NSLog(u"UI for window %s with options: %s" % (window.title(), u'|'.join(options)))
    
    button = WindowButton.alloc().initWithTargetWindow_(window)
    button.setAutoresizingMask_(NSViewMaxXMargin|NSViewMinYMargin)
    windowFrameView = window.contentView().superview()
    if windowFrameView is not None:
        if windowFrameView.__class__.__name__ == 'MGCinematicFrameView':
            for subview in windowFrameView.subviews():
                if subview.__class__.__name__ == 'MGCinematicFrameTitlebarView':
                    windowFrameView = subview
                    button.setFrame_(NSMakeRect(70, 4, 15, 17)) # Magic
                    break
        windowFrameView.addSubview_(button)
        WindowEmitter.sharedSender().addButton_forWindow_(button, window)

def addGlassPaneForWindow_(window):
    if isinstance(window, (GlassWindow, )):
       return
    
    # if WindowEmitter.sharedSender().glassPaneForWindow_(window):
    #     # Window already has a glass pane
    #     return
        
    if not (window.styleMask() & (NSClosableWindowMask|NSMiniaturizableWindowMask|NSResizableWindowMask)):
        # Only add glass pane to titled windows  -- WHY? -JRE 2010-02-21
        return
        
    glassPane = GlassWindow(window)
    # glassPane.setBackgroundColor_(NSColor.colorWithCalibratedRed_green_blue_alpha_(0, 1, 0, 0.2))
    WindowEmitter.sharedSender().addGlassPane_forWindow_(glassPane, window)
    #window.addChildWindow_ordered_(glassPane, NSWindowAbove)
    # print window.isVisible()
    #     if window.isVisible():
    #         window.orderFront_(window)