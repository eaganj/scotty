# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup
import copy
import glob
from modulegraph.find_modules import find_modules, parse_mf_results
import os
from plistlib import Plist
import py2app
from py2app.filters import not_stdlib_filter, not_system_filter, has_filename_filter
import shutil
import sys

plist = dict(NSPrincipalClass=u'ScottySIMBLInjector',
             CFBundleIdentifier=u'fr.lri.eaganj.Scotty',
             SIMBLTargetApplications= [ dict(BundleIdentifier=u'com.apple.Dictionary',
                                             MaxBundleVersion=u'120',
                                             MinBundleVersion=u'50',
                                           ),
                                         dict(BundleIdentifier=u'org.videolan.vlc',
                                              MaxBundleVersion=u'0.9.9-15-gd754503',
                                              MinBundleVersion=u'0.9.9-15-gd754503',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.Safari',
                                              MaxBundleVersion=u'9532',
                                              MinBundleVersion=u'5530',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.QuickTimePlayerX',
                                              MaxBundleVersion=u'600',
                                              MinBundleVersion=u'50',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.dock',
                                              MaxBundleVersion=u'70',
                                              MinBundleVersion=u'100',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.finder',
                                              MaxBundleVersion=u'10.6',
                                              MinBundleVersion=u'10.6',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.iWork.Keynote',
                                              MaxBundleVersion=u'1200',
                                              MinBundleVersion=u'200',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.iWork.Pages',
                                              MaxBundleVersion=u'1000',
                                              MinBundleVersion=u'200',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.Terminal',
                                              MaxBundleVersion=u'280',
                                              MinBundleVersion=u'270',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.Preview',
                                              MaxBundleVersion=u'800',
                                              MinBundleVersion=u'501',
                                           ),
                                         dict(BundleIdentifier=u'com.google.Chrome',
                                              MaxBundleVersion=u'400',
                                              MinBundleVersion=u'249.30',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.mail',
                                              MaxBundleVersion=u'2000',
                                              MinBundleVersion=u'1081',
                                           ),
                                         dict(BundleIdentifier=u'me.eagan.macirc',
                                              MaxBundleVersion=u'9999',
                                              MinBundleVersion=u'1',
                                           ),
                                         dict(BundleIdentifier=u'com.skype.skype',
                                              MaxBundleVersion=u'9999',
                                              MinBundleVersion=u'1',
                                           ),
                                         dict(BundleIdentifier=u'com.bittorrent.uTorrent',
                                              MaxBundleVersion=u'99999',
                                              MinBundleVersion=u'1',
                                           ),
                                         dict(BundleIdentifier=u'com.apple.iCal',
                                              MaxBundleVersion=u'99999',
                                              MinBundleVersion=u'1547',
                                           ),
                                     ],
            )

def not_scottylib_filter(module):
    return os.path.dirname(module.filename) not in (
                                    os.getcwd(), 
                                    os.path.normpath(os.path.join(os.getcwd(), os.pardir, 'Common'),
                                    ))

def my_not_stdlib_filter(module):
    return '/site-python/' not in module.filename and '/site-packages/' not in module.filename

def get_script_modules(path):
    sys.path.append(os.path.dirname(path))
    mf = find_modules(scripts=[path])
    filters = [has_filename_filter, not_stdlib_filter, not_system_filter, 
               not_scottylib_filter, my_not_stdlib_filter, 
              ]
    mf.filterStack(filters)
    pyFiles, extensions = parse_mf_results(mf)
    return [ module.filename for module in pyFiles ]
    
def makeInstrumentPlugin(name, srcDir, entryPoints, resources):
    buildDir = u'build'
    pluginDir = os.path.join(buildDir, 'Instruments', name)
    if os.path.exists(pluginDir):
        #print 'clobbering', pluginDir
        if os.path.islink(pluginDir):
            os.unlink(pluginDir)
        elif os.path.isdir(pluginDir):
            shutil.rmtree(pluginDir)
        else:
            os.unlink(pluginDir)
            
    print "** building plugin :", name, "**"
    contentDir = os.path.join(pluginDir, 'Contents')
    resourceDir = os.path.join(contentDir, 'Resources')
    #print "mkdirs", resourceDir
    os.makedirs(resourceDir)
    pyFiles = set()
    for entryPoint in entryPoints:
        # Run modulegraph to figure out what we need to include
        pyFiles.update(get_script_modules(os.path.join(srcDir, 'Contents', 'Resources', entryPoint)))
        
    print "Copying", len(pyFiles), "python file%s" % ("" if len(pyFiles) == 1 else "s")
    for src in pyFiles:
        #print "copy", src, resourceDir
        shutil.copy2(src, resourceDir)
    
    for rsrc in resources:
        if os.path.isdir(rsrc):
            #print "copytree", rsrc, os.path.join(resourceDir, os.path.basename(rsrc))
            shutil.copytree(rsrc, os.path.join(resourceDir, os.path.basename(rsrc)))
        else:
            #print "copy", rsrc, resourceDir # use shutil.copy2
            shutil.copy2(rsrc, resourceDir)
    
    shutil.copy2(os.path.join(srcDir, 'Contents', 'Info.plist'), contentDir)
    
    return pluginDir

def makeInstrumentPluginAlias(name, srcDir, entryPoints, resources):
    buildDir = u'build'
    pluginDir = os.path.join(buildDir, 'Instruments', name)
    if os.path.exists(pluginDir):
        #print 'clobbering', pluginDir
        if os.path.islink(pluginDir):
            os.unlink(pluginDir)
        elif os.path.isdir(pluginDir):
            shutil.rmtree(pluginDir)
        else:
            os.unlink(pluginDir)
    
    print "** building plugin ->", name, "**"
    if not os.path.exists(os.path.join('build', 'Instruments')):
        os.makedirs(os.path.join('build', 'Instruments'))
    
    os.symlink(os.path.abspath(srcDir), pluginDir)
    
    return pluginDir

def getInstrumentMaker():
    in_py2app = False
    for arg in sys.argv:
        if arg == 'py2app':
            in_py2app = True
        elif in_py2app:
            if arg == '-A':
                return makeInstrumentPluginAlias
    
    return makeInstrumentPlugin
    
def makeInstrumentPlugins():
    plugins = []
    
    makeInstrument = getInstrumentMaker()
    for pluginDir in glob.glob(os.path.join('Instruments', '*.instrument')):
        plist = Plist.fromFile(os.path.join(pluginDir, 'Contents', 'Info.plist'))
        #pyFiles = glob.glob(os.path.join(pluginDir, 'Contents', 'Resources', '*.py'))
        entryPoints = [ plist['IIKitInstrumentScriptName'] ]
        resources = []
        resourcesDir = os.path.join(pluginDir, 'Contents', 'Resources')
        if os.path.exists(resourcesDir):
            resources = glob.glob(os.path.join(resourcesDir, '*'))
        
        plugin = makeInstrument(os.path.basename(pluginDir), pluginDir, entryPoints, resources)
        plugins.append(plugin)
    
    return plugins

def repackageInstrumentPlugins(pluginDir):
    srcPath = os.path.join(pluginDir, u"Contents", u"Resources", u"Instruments")
    pluginsPath = os.path.join(pluginDir, u"Contents", u"PlugIns")
    dstPath = os.path.join(pluginsPath, u"Instruments")
    # if os.path.islink(srcPath):
    #     linkPath = srcPath
    #     srcPath = os.path.realpath()
    # shutil.move(srcPath, dstPath)
    
    if not os.path.exists(pluginsPath):
        os.makedirs(pluginsPath)
    if os.path.exists(dstPath):
        if os.path.islink(dstPath):
            os.unlink(dstPath)
        elif os.path.isdir(dstPath):
            shutil.rmtree(dstPath)
            
    os.renames(srcPath, dstPath)


instrumentPlugins = makeInstrumentPlugins()

alias_build_files = glob.glob('*.pth')
injector = setup(
    plugin = [ 'ScottySIMBLInjector.py'],
    data_files=glob.glob('Plugin NIBs/*[!~].nib') + glob.glob('Plugin Resources/*[!~]') + alias_build_files +\
               [
                    # Add stuff here
                    u'build/Instruments',
                    # Need to include standard library, too! (e.g. Instruments that aren't imported
                    # directly by Scotty but which may be used by instrument plugins.)
                    # FIXME: todo
                    # u'../../../jre',
                    # u'../../State Machines/StateMachines',
                    #u'Instruments',

# These should not be necessary!
                    # u'/System/Library/Frameworks/Python.framework/Versions/2.6/lib/python2.6/contextlib.pyc',
                    # u'/System/Library/Frameworks/Python.framework/Versions/2.6/Extras/lib/python/PyObjC/PyObjCTools',
                    # u'../../iStar/istar',
               ],
    options=dict(py2app=dict(extension='.bundle', 
                             plist=plist,
                             includes=[u'ScottyProtocol',
                                       u'WidgetPickerInstrument',
                                      ],
                             )),
)

repackageInstrumentPlugins(injector.plugin[0].appdir)

# >>> from modulegraph.find_modules import *                                      
# >>> mf = find_modules(scripts=['/Users/eaganj/Projects/LRI/Scotty/Injector/Instruments/WidgetPicker.instrument/Contents/Resources/WidgetPickerInstrument.py'])
# >>> from py2app.filters import not_stdlib_filter, not_system_filter, has_filename_filter
# >>> filters = [has_filename_filter, not_stdlib_filter, not_system_filter]
# >>> mf.filterStack(filters)
# (287, 264, 1)
# >>> py_files, extensions = parse_mf_results(mf)>>> len(py_files)
# 23
# >>> len(extensions)
# 0
# >>> 
# >>> filenames = set([ pf.filename for pf in py_files ])
# >>> scottymf = find_modules(scripts=['/Users/eaganj/Projects/LRI/Scotty/Injector/ScottySIMBLInjector.py'])>>> scottymf.filterStack(filters)(288, 264, 1)
# >>> scotty_py_files, scotty_extensions = parse_mf_results(scottymf)>>> len(scotty_py_files)
# 24
# >>> scotty_filenames = set([ pf.filename for pf in scotty_py_files ])           >>> plugin_filenames = set([ fn for fn in filenames if fn not in scotty_filenames ])
