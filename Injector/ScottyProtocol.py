# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

# from Foundation import *
# from AppKit import *
# import objc

import jre.cocoa
import jre.debug

class ScottyProtocol(object):
    kind = []
    signature = []
    
    # FIXME: bindings are currently memoized by object class, which means that a class' objects can either
    # always conform or never conform; conformance can not change over time, e.g. as its state changes.
    _bindings = {}
    
    @classmethod
    def objectConforms_(cls, obj):
        # TODO: implement binding support
        result = cls._bindings.get(obj.__class__, None)
        if result is not None:
            return result
            
        for k in cls.kind:
            if isinstance(obj, k):
                cls._bindings[obj.__class__] = True
                return True 
        
        # Object does not match any type signatures.  Try method signatures instead.
        for s in cls.signature:
            for methodList in s:
                methodList = methodList if isinstance(methodList, list) else [ methodList ]
                valid = False
                for method in methodList:
                    valid = valid or hasattr(obj, method)
                if not valid:
                    break # This signature does not match
            else:
                # Object matches all signatures
                cls._bindings[obj.__class__] = True
                print 'matched signature', obj.__class__
                return True
                    
        # No matching signature found.
        cls._bindings[obj.__class__] = False
        return False
    
    def bindObject_(self, obj):
        # TODO : implement proper binding support
        return obj

Protocol = ScottyProtocol