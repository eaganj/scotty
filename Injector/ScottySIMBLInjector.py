# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from AppKit import *
from Foundation import *
import objc

class ScottySIMBLInjector(NSObject):
    _sharedInstance = None
    
    @classmethod
    def initialize(cls):
        plugin = cls.sharedInstance() # initialize shared instance
        
        plugin.showInjectionDialogIfNecessary()
    
    @classmethod
    def sharedInstance(cls):
        if cls._sharedInstance is None:
            cls._sharedInstance = cls.alloc().init()
        
        return cls._sharedInstance
    
    def showInjectionDialogIfNecessary(self):
        defaults = NSUserDefaults.standardUserDefaults()
        shouldInject = defaults.stringForKey_(u'fr.lri.eaganj.Scotty.ShouldInject')
        if not shouldInject:
            alert = NSAlert.alertWithMessageText_defaultButton_alternateButton_otherButton_informativeTextWithFormat_(
            u"Install Scotty Support?",
            u"Install",
            u"Don't Install",
            None,
            u"Installing Scotty support adds the Scotty button to your windows."
            )
            alert.setAlertStyle_(NSInformationalAlertStyle)
            alert.setShowsSuppressionButton_(True)
            alert.buttons()[1].setKeyEquivalent_(u'\033') # UGLY -- I can't find a constant for Esc
            result = alert.runModal()
            if result == NSAlertDefaultReturn:
                shouldInject = u'yes'
            shouldInject = u'yes' if result == NSAlertDefaultReturn else u'no'
            
            if alert.suppressionButton().state() == NSOnState:
                defaults.setObject_forKey_(shouldInject, u'fr.lri.eaganj.Scotty.ShouldInject')
        
        if shouldInject.lower() == u'yes':
            NSLog(u"Installing scotty support")
            try:
                import ScottyPlugin
            except Exception, e:
                import sys, traceback
                traceback.print_exception(*sys.exc_info())
                
                alert = NSAlert.alertWithMessageText_defaultButton_alternateButton_otherButton_informativeTextWithFormat_(
                u"Error installing Scotty",
                u"OK",
                None,
                None,
                u"An error occurred installing Scotty: {}: {}".format(e.__class__.__name__, e)
                )
                alert.setAlertStyle_(NSInformationalAlertStyle)
                # alert.setShowsSuppressionButton_(True)
                # alert.buttons()[1].setKeyEquivalent_(u'\033') # UGLY -- I can't find a constant for Esc
                result = alert.runModal()