# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

import platform

import jre.cocoa
import jre.debug

#from InstrumentManager import *
from IIKit.scotty import *
from ScottyController import *

BUTTON_SIZE = 15, 15
BUTTON_ORIGIN = 70, 4

ModuleBundle = objc.currentBundle()

def loadImage_(path):
    return NSImage.alloc().initWithContentsOfFile_(ModuleBundle.pathForImageResource_(path))
    
class ScottyWindowButton(NSButton):
# class ScottyWindowButton(NSSegmentedControl):
    targetWindow = objc.ivar()
    focusedImage = loadImage_(u"greybutton-trans.png")
    unfocusedImage = loadImage_(u"greybutton-disabled-trans")
    focusedActiveImage = loadImage_(u"greybutton-active-trans")
    unfocusedActiveImage = loadImage_(u"greybutton-disabled-active-trans")
    
    imageMap = [ [ unfocusedImage, unfocusedActiveImage ],
                 [ focusedImage, focusedActiveImage ] ]
    
    def initWithTargetWindow_(self, window):
        ((winX, winY), (winW, winH)) = window.frame()
        buttonFrame = NSMakeRect(BUTTON_ORIGIN[0], winH - BUTTON_ORIGIN[1] - BUTTON_SIZE[1],
                                 BUTTON_SIZE[0], BUTTON_SIZE[1])
        self = super(ScottyWindowButton, self).initWithFrame_(buttonFrame)
        if not self:
            return self
                
        self.targetWindow = window
        
        self.setButtonType_(NSMomentaryChangeButton)
        self.setTitle_(u"")
        self.setBordered_(False)
        
        # self.setSegmentCount_(1)
        # self.setLabel_forSegment_(u'', 0)
        
        self.updateImage()
        self.setTarget_(self)
        self.setAction_(self.toggleWindowButton_)
        
        self._initScottyMenu()
        
        return self
    
    def _initScottyMenu(self):
        menu = NSMenu.alloc().initWithTitle_(u"Scotty")
        
        for instrument in InstrumentManager.sharedInstrumentManager().instruments():
            instrumentID = instrument.instrumentID
            title = instrument.verb or instrument.name or instrumentID[instrumentID.find('/')+1:]
            item = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_(title, 'activateInstrument:', '')
            item.setTarget_(self)
            item.setRepresentedObject_(instrumentID)
            menu.addItem_(item)
        
        # Add Preferences
        menu.addItem_(NSMenuItem.separatorItem())
        item = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_(u"Scotty Preferences...", 
                                                                      "showPreferences:", 
                                                                      u"")
        item.setTarget_(Scotty())
        menu.addItem_(item)
        self._scottyMenu = menu
        self._showingMenu = False
        
    def toggleWindowButton_(self, sender):
        emitter = Scotty().sharedSender()
        emitter.enableHook_forWindow_(self.state() == NSOnState, self.targetWindow)
        self.updateImage()
    
    @jre.debug.trap_exceptions
    def activateInstrument_(self, sender):
        instrumentID = sender.representedObject()
        context = InstrumentContext()
        context.activationSource = self
        Scotty().activateInstrument_withContext_(instrumentID, context)
    
    def updateImage(self):
        image = self.imageMap[ self.targetWindow.isMainWindow() ][ self.state() == NSOnState ]
        self.setImage_(image)
    
    def isOpaque(self):
        return False
    
    def showScottyMenu_(self, event):
        self._showingMenu = True
        if platform.mac_ver()[0].startswith('10.5'):
            # Leopard
            NSMenu.popUpContextMenu_withEvent_forView_(self._scottyMenu, event, self.superview())
        else:
            # - NSMenu popUpMenuPositioningItem:atLocation:inView: was added in 10.6 ; use it
            self._scottyMenu.popUpMenuPositioningItem_atLocation_inView_(None, (0, 20), self)
        self.cell().setHighlighted_(False)
    
    def mouseDown_(self, event):
        self.cell().setHighlighted_(True)
        self._showingMenu = False
        self.performSelector_withObject_afterDelay_(self.showScottyMenu_, event, 0.25)
        #performSelector:withObject:afterDelay:
        #cancelPreviousPerformRequestsWithTarget:selector:object:
        #cancelPreviousPerformRequestsWithTarget:
    
    def mouseUp_(self, event):
        NSObject.cancelPreviousPerformRequestsWithTarget_(self)
        point = event.locationInWindow()
        inFrame = NSMouseInRect(point, self.frame(), False)
        self.cell().setHighlighted_(False)
        if inFrame and not self._showingMenu:
            self.setState_(not self.state())
            self.sendAction_to_(self.action(), self.target())
    
    def mouseDragged_(self, event):
        point = event.locationInWindow()
        inFrame = NSMouseInRect(point, self.frame(), False)
        self.cell().setHighlighted_(inFrame)

WindowButton = ScottyWindowButton