# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from AppKit import *
from Foundation import *
import objc

import jre.debug

from DecoratorWindowView import DecoratorWindowView

from collections import deque

class ScottyDecoratorWindow(NSWindow):
    def __new__(cls, parent):
        return cls.alloc().initWithParent_(parent)
        
    @jre.debug.trap_exceptions
    def initWithParent_(self, parent):
        self = super(ScottyDecoratorWindow, self).initWithContentRect_styleMask_backing_defer_(
                                                        self.frameRectForParentFrameRect_(parent.frame()),
                                                        NSBorderlessWindowMask,
                                                        NSBackingStoreBuffered,
                                                        False
                                                    )
        if not self:
            return self
        
        self.setHasShadow_(True)
        self.setOpaque_(False)
        self.setIgnoresMouseEvents_(False)
        self.setDelegate_(self)
        NSNotificationCenter.defaultCenter().addObserver_selector_name_object_(
                                                                           self,
                                                                           self.parentDidResize_,
                                                                           NSWindowDidResizeNotification,
                                                                           parent,
                                                                      )
                                                          
        frame = self.frame()
        parent.addChildWindow_ordered_(self, NSWindowBelow)
        self._contentView = NSView.alloc().initWithFrame_(frame)
        decoratorPanel = DecoratorWindowView(NSMakeRect(2, 0,
                                                       frame.size.width-2, frame.size.height))
        self._contentView.addSubview_(decoratorPanel)
        self.setContentView_(self._contentView)
        
        # cvFrame = self.contentView().frame()
        # self.contentView().setFrame_(NSMakeRect(cvFrame.origin.x + 4, cvFrame.origin.y,
        #                                         cvFrame.size.width-4, cvFrame.size.height))
        
        return self
        
    @jre.debug.trap_exceptions
    def parentDidResize_(self, notification):
        self.setFrame_display_(self.frameRectForParentFrameRect_(notification.object().frame()), True)
    
    def scottyParentWindowWasReordered_relativeTo_(self, order, windowID):
        # We need this hack to make sure we receive an initial window ordering message when our
        # parent is made visible.  This hack relies on a category addition to NSWindows and a
        # swizzled-in overload to the NSWindow.orderWindow_relativeTo_ method that calls our
        # category method.
        #
        # This hack is necessary because our window appears not to receive mouse events until it
        # has received such an order message, even though the parent window takes over the ordering
        # of its child windows.  (Tested on OS X 10.5.8 and 10.6.0).
        if order != NSWindowOut:
            self.orderWindow_relativeTo_(NSWindowBelow, self.parentWindow().windowNumber())
            
    @jre.debug.trap_exceptions
    def frameRectForParentFrameRect_(self, frame):
        return NSMakeRect(frame.origin.x + frame.size.width - 4, frame.origin.y + frame.size.height - 90, 
                          22+4, 90)

    @jre.debug.trap_exceptions
    def mouseDown_(self, event):
        jre.debug.TRACE()
        print 'mouse down 3'
      
    @jre.debug.trap_exceptions
    def dealloc(self):
        NSNotificationCenter.defaultCenter().removeObserver_(self)

DecoratorWindow = ScottyDecoratorWindow

__all__ = 'ScottyDecoratorWindow DecoratorWindow'.split()