# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from collections import deque
from functools import wraps
import hashlib
import repr
import select
import socket
import struct
import sys
import threading
import time

import jre.debug

try:
    # We're running on a Mac -- load PyObjC integration support
    from Foundation import *
    import jre.cocoa # For NSAutoreleasePool ContextManager category additions
    
    class Thread(threading.Thread):
        def _Thread__bootstrap(self):
            ''' Ugly ugly ugly hack to suppress an autorelease memory leak '''
            # FIXME: works in Python 2.5.  Might not work in later versions.  However, in those later
            #        versions this should "only" cause a small memory leak on each thread invocation.
            with NSAutoreleasePool.alloc().init():
                super(ScottyWindowEmitterWorker, self)._Thread__bootstrap()
except ImportError:
    # We're not running in PyObjC
    Thread = threading.Thread
    class NSAutoreleasePool(object):
        def alloc(self):
            return self
        def init(self):
            return self
        def __enter__(self):
            return self
        def __exit__(self, ext, exv, extb):
            return False

try:
    RBUFHACK = False
    if isinstance(socket._fileobject(None)._rbuf, list):
        RBUFHACK = True
except:
    pass

import jre.cocoa
import jre.debug

class SocketConnectionError(Exception):
    pass

# FIXME: refactor this to be a metaclass
class SocketConnection(threading.Thread):
    def __init__(self, connection, protocol, delegate=None):
        ''' A Protocol mixin for a non-blocking, threaded protocol connection over a socket.
        
            `connection` may be either an already-connected socket, address pair (as might be received
                         from `socket.accept()`) or a `(host, port)` pair which will be used to create
                         a standard TCP/IP socket.
            
            This connection object will manage blocking for incoming data and send outgoing data via the
            protocol.
            
            Usage: class MyProtocol(BinaryProtocol, SocketConnection)
        '''
        
        super(SocketConnection, self).__init__()
        
        # socket/connection management
        if isinstance(connection, tuple) and isinstance(connection[0], socket.socket):
            self._sc_socket, (self._sc_host, self._sc_port) = connection
            self._sc_initSocket(self._sc_socket)
        else:
            self._sc_host, self._sc_port = connection
            self._sc_socket = None

        self._sc_socketErrorFlag = False
        self._sc_protocol = protocol
        self._sc_prepareProtocol(protocol)

        # Buffer management
        self._sc_bufferLock = threading.RLock()
        self._sc_bufferFlag = threading.Event()
        self._sc_buffer = deque()
        self._sc_discard_helper = {}
        self._sc_acks = set()
        
        # Thread management
        self._sc_shouldDie = False
        
        # Delegate
        self._sc_delegate = delegate
        
        return self

    def _sc_prepareProtocol(self, protocol):
        ''' Process the protocol by replacing all send* methods with a buffered variant. '''
        for methodName in dir(protocol):
            if methodName.startswith('send'):
                # Replace all send* methods with a buffered variant
                method = getattr(protocol, methodName)
                if hasattr(self, methodName):
                    # Defer existing method
                    todefer = getattr(self, methodName)
                    todefer.__dict__.update(method.__dict__)
                    setattr(self, methodName, 
                            self._sc_methodWrapper(todefer, nostream=True))
                    assert not hasattr(method, '__discard') or \
                           hasattr(getattr(self, methodName), '__discard')
                    setattr(self, '_do%s%s' % (methodName[0].upper(), methodName[1:]), 
                            wraps(method)(lambda *args, **kwargs: method(self._sc_wsocket, 
                                                                         *args, 
                                                                         **kwargs)))
                else:
                    setattr(self, methodName, self._sc_methodWrapper(method))

    def _sc_methodWrapper(self, method, nostream=False):
        @wraps(method)
        def _sc_bufferedSender(*args, **kwargs):
            # We add the outgoing socket as the stream parameter to the call, removing the need for the
            # caller to include it.
            # Prepare message
            
            # if not nostream and not hasattr(self, '_sc_wsocket'):
            #     if self._sc_delegate and hasattr(self._sc_delegate, 'socketConnectionError'):
            #         self._sc_delegate.socketConnectionError(self, 
            #                                                 SocketConnectionError(u'not connected'))
            #     else:
            #         raise SocketConnectionError(u'not connected')
            
            with self._sc_bufferLock:
                # if len(args) > 1:
                #     print 'buf sendMessage  with', len(args[1]), "B  (%s)" % (hashlib.md5(args[1]).hexdigest()), id(args[1])
                    
                message = self._sc_makeMessageHandle(method, args, kwargs)
                self._sc_buffer.append(message)
            
            self._sc_bufferFlag.set()
            
            # if nostream:
            #     pass
            # elif not hasattr(self, '_sc_wsocket'):
            #     if self._sc_delegate and hasattr(self._sc_delegate, 'socketConnectionError'):
            #         self._sc_delegate.socketConnectionError(self, 
            #                                                 SocketConnectionError(u'not connected'))
            #     else:
            #         raise SocketConnectionError(u'not connected')
            # else:
            #     # insert write socket into args
            #     #args = (self._sc_wsocket,) + args
            #     pass
            # 
            # with self._sc_bufferLock:
            #     message = (method, args, kwargs)
            #     if getattr(method, '__discard', False):
            #         self._sc_discard_helper[method] = message
            #         # enqueue a pointer to the most recent message instead of the message itself
            #         message = [ self._sc_discard_helper, method ] # UGLY: we rely on this not being a tuple
            #     
            #     self._sc_buffer.append(message)
            #     
            # self._sc_bufferFlag.set()

        
        return _sc_bufferedSender
    
    def _sc_makeMessageHandle(self, method, args, kwargs):
        if getattr(method, '__discard', False):
            self._sc_discard_helper[method] = (method, args, kwargs)
            message = [ self._sc_discard_helper, method ] # UGLY: we rely on this not being a tuple
            
            data = self._sc_discard_helper[method][1][1]
            # print 'prepared message with', len(data), "B  (%s)" % (hashlib.md5(data).hexdigest()), id(data)
        else:
            message = (method, args, kwargs)
        
        return message
    
    def _sc_verifyConnection(self):
        if not self._sc_socket or self._sc_socketErrorFlag:
            try:
                if not self._sc_host or not self._sc_port:
                    self._sc_shouldDie = True
                    if self._sc_socket:
                        raise SocketConnectionError('Lost connection to %s' %
                                                                        (self._sc_socket.getpeername()))
                    else:
                        raise SocketConnectionError('Lost connection to %s '
                                                    '(unable to re-establish)' % (self._sc_host))
                        
                self._sc_socket = socket.socket()
                self._sc_socket.connect((self._sc_host, self._sc_port))
                self._sc_initSocket(self._sc_socket)
            except socket.error, e:
                self._sc_socketErrorFlag = True
                raise SocketConnectionError("Could not connect to %s:%s: %s (%s)" %
                                                    (self._sc_host, self._sc_port, e.args[1], e.args[0]))
    
    def _sc_initSocket(self, socket):
        self._sc_rsocket = self._sc_socket.makefile('rb')
        self._sc_wsocket = self._sc_socket.makefile('wb')
        self._sc_socketErrorFlag = False
    
    def _sc_cleanup(self):
        del self._sc_socket
        if hasattr(self, '_sc_rsocket'):
            del self._sc_rsocket
        if hasattr(self, '_sc_wsocket'):
            del self._sc_wsocket
    
    def run(self):
        with NSAutoreleasePool.alloc().init():
            try:
                while not self._sc_shouldDie:
                    self._sc_verifyConnection()
                    with NSAutoreleasePool.alloc().init():
                        self._sc_processIncomingStream()
                        self._sc_processOutgoingStream() # May block for up to 0.20 s
            except (SocketConnectionError, IOError), e:
                if self._sc_delegate and hasattr(self._sc_delegate, 'socketConnectionError'):
                    self._sc_delegate.socketConnectionError(self, e)
                else:
                    print "%s: %s" % (e.__class__.__name__, e)
            except Exception, e:
                jre.debug.printStackTrace()
                if self._sc_delegate and hasattr(self._sc_delegate, 'socketConnectionError'):
                    self._sc_delegate.socketConnectionError(self, e)
                else:
                    raise # ignore and re-raise
            else:
                if self._sc_delegate and hasattr(self._sc_delegate, 'socketConnectionClosed'):
                    self._sc_delegate.socketConnectionClosed(self)
            finally:
                self._sc_cleanup()
    
    def _sc_getCurrentMessageID(self):
        return self._sc_protocol.currentMessageID
    
    _sc_currentMessageID = property(_sc_getCurrentMessageID)
    
    def _sc_processIncomingStream(self):
        while True:
            rready = select.select([self._sc_rsocket], [], [], 0.0)[0]
            # We need to check not only the result of the select call but also the *private* read buffer
            # of the socket's fileobject (see _fileobject in socket.py).  This is an ugly hack to work
            # around the fact that socket.py keeps its own readbuffer (without a public interface to see
            # if it's available for reading!).  This buffer might have unconsumed data that it recv()d
            # from the socket but which was not read() from the _fileobject.  In this case, select will
            # (correctly) respond that the socket is not available for reading, but there will still be
            # buffered data that has not yet been read!
            # Note: The version of _fileobject in Python 2.6 ensures that the private readbuffer is always
            # left empty, alleviating the need for this hack in 2.6 and beyond.
            if not rready and (not RBUFHACK or len(self._sc_rsocket._rbuf) < 4):
                break
            
            #sys.stdout.write('.'); sys.stdout.flush()
            self._sc_protocol.feed(self._sc_rsocket)
    
    def _sc_processOutgoingStream(self):
        self._sc_bufferFlag.wait(0.20) # Ugly hack.  Wait to make sure we have data ready.
        if not self._sc_bufferFlag.isSet():
            return # Basically this makes the loop do busy polling. :-(
        
        # print len(self._sc_buffer),
        # sys.stdout.flush()
        
        with self._sc_bufferLock:
            self._sc_bufferFlag.clear()
            toSend, toHold = self._sc_processOutgoingBuffer()
            
            for message in toHold:
                self._sc_buffer.append(message)
        
        # We no longer hold lock
        for method, args, kwargs in toSend:
            args = (self._sc_wsocket,) + args
            try:
                method(*args, **kwargs)
            except Exception, e:
                ppargs = u', '.join(map(repr.repr, args))
                ppkwargs = u''.join([', %s=%s' % (repr.repr(k),repr.repr(v)) for k,v in kwargs.items()])
                print "Error sending message: %s(%s%s) via %s" % (method.__name__, ppargs, ppkwargs, repr.repr(method))
                raise # re-raise
        
        self._sc_wsocket.flush()
    
    def _sc_processOutgoingBuffer(self):
        '''
            Process the items in the outgoing buffer returning `(toSend, held)`, where `toSend` is
            the list of messages ready to be sent across the stream and `held` is the list of messages
            to be held in the queue (e.g. because they are held pending an ACK to a previous message.).
            
            NOTE:  If any locks must be acquired to access the queue, they should already be held before
                   calling this method.
        '''
        toSend = []
        heldMessages = []
        while self._sc_buffer:
            message = self._sc_buffer.popleft()
            
            # Discard stale __discarded messages as appropriate
            message = self._sc_processDiscardMessage(message)
            if not message:
                continue
            
            # Check if message should be held for an ACK
            if not self._sc_processAckedMessage(message):
                heldMessages.append(message)
                # print '...'
                continue
            
            toSend.append(message)
        
        for message in heldMessages:
            message = self._sc_makeMessageHandle(*message)
        
        return toSend, heldMessages
        
    def _sc_processDiscardMessage(self, message):
        ''' Process message, returning the appropriate message to send or 
            None if the message should be discarded.
        '''
        if not isinstance(message, tuple):
            discardHelper, key = message
            if key in discardHelper:
                message = discardHelper[key]
                del discardHelper[key]
                data = message[1][1]
                # print 'popped a message with', len(data), "B  (%s)" % (hashlib.md5(data).hexdigest()), id(data)
            else:
                # print 'x'
                message = None
        
        return message
    
    def _sc_processAckedMessage(self, message):
        ''' If the message corresponds to an ack'd message type, clear any acks and return True if the
            message should be sent.  Returns False if the message should be held.
            
            For non-Ackd messages, always returns True.
        '''
        # Unpack message
        method, args, kwargs = message
        
        ackType = getattr(method, '__waitForAck', False)
        if ackType:
            if ackType in self._sc_protocol._receivedAcks:
                # Clear ack
                self._sc_protocol._receivedAcks.remove(ackType)
            else:
                # hold message
                return False
        
        return True