# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from functools import wraps
import hashlib
import struct
import sys

_FLAGS = ('!', '<', '>')
class BinaryMessage(object):
    def __init__(self, messageID, signature, byteOrder='!'):
        ''' `signature` is a list containing entities in the form `x:name` where name is a parameter
                        of a message and x is the struct format type for the message (see `struct` 
                        Python module).
            `byteOrder`, if specified, determines the byte order expected for sent and received messages.
                        By default, network (big-endian) byte order is used.  The symbols `@!<>` are 
                        accepted here.  See `struct` module for details.
        '''
        dynamic = False
        format = []
        for descriptor in signature:
            argType, argName = descriptor.split(':')
            dynamic = dynamic or (argType == 'o')
            format.append(argType)
        
        self._prefix = byteOrder
        self._format = format
        self._messageID = messageID
        self._struct = struct.Struct(''.join([self._prefix] + self._format)) if not dynamic else None
        
    
    def _makeStruct(self, args):
        assert len(self._format) == len(args) + 1, \
               'Arguments to message do not match expected format: %s' % (
                                                            ''.join([self._prefix] + self._rformat))
               
        format = ['i']
        newArgs = [self._messageID]
        for arg, argType in zip(args, self._format):
            if argType == 'o':
                format.append('i%sB' % (len(arg)))
                newArgs.append(str(len(arg)))
                newArgs.append(arg)
            else:
                format.append(argType)
                newArgs.append(arg)
        
        return struct.Struct(''.join([self._prefix] + format)), newArgs
        
    def send(self, stream, *args):
        #ppargs = [ 'data<%s>' % len(arg) if hasattr(arg, '__len__') and len(arg) > 10 else arg
        #                for arg in args ]
        #print 'Sending to %s: %s %s' % (repr(stream), hex(self._messageID), repr(ppargs))
        stream.write(struct.pack('%si' % (self._prefix), self._messageID))
        
        # if self._messageID == 0x22 and len(args) > 1:
        #     print '--> updateWindow with', len(args[1]), "B  (%s)" % (hashlib.md5(args[1]).hexdigest()), id(args[1])
        #     print repr(args[1])
        #     print
            
        if self._struct:
            stream.write(self._struct.pack(*args))
        else:
            self._dynamicSend(stream, *args)
    
    def _dynamicSend(self, stream, *args):
        format = []
        toSend = []
            
        for arg, argType in zip(args, self._format):
            if argType == 'o':
                format.append('i')
                toSend.append(len(arg))
                stream.write(struct.pack(''.join([self._prefix] + format), *toSend))
                format = []
                toSend = []
                stream.write(arg)
                
            else:
                format.append(argType)
                toSend.append(arg)
        
        if toSend:
            stream.write(struct.pack(''.join([self._prefix] + format), *toSend))
        
    def receive(self, stream):
        if self._struct:
            if not self._struct.size:
                return []
            data = stream.read(self._struct.size)
            if not data:
                raise IOError("Connection reset by peer")
                
            return self._struct.unpack(data)
            
        format = []
        result = []
        for argType in self._format:
            if argType == 'o':
                result += self._doReceive(stream, format)
                format = []
                data = stream.read(4)
                if not data:
                    raise IOError("Connection reset by peer")
                taille = struct.unpack('%si' % (self._prefix), data)[0]
                #format.append('%sB' % (taille))
                result.append(stream.read(taille))
            else:
                format.append(argType)
        
        if format:
            result += self._doReceive(stream, format)
        return result
    
    def _doReceive(self, stream, formatList):
        format = ''.join([self._prefix] + formatList)
        data = stream.read(struct.calcsize(format))
        if not data:
            raise IOError("Connection reset by peer")
            
        return struct.unpack(format, data)
        
def _registerCallback(callback, messageID, signature, options):
    #This is an ugly hack to look into the caller's context, which should
    # be the the context of the class in which the decorator was called.
    local = sys._getframe(2).f_locals
    handlers = local.setdefault('_handlers', {})
    pairedAcks = local.setdefault('_pairedAcks', {})

    messageObject = BinaryMessage(messageID, signature)
    handlers[messageID] = (callback, messageObject)
    
    if 'waitForAck' in options:
        ackID = options['waitForAck']
        pairedAcks[ackID] = messageID
        
        
    return messageObject

def _addSenderMethod(messageName, messageObject, **options):
    #This is an ugly hack to look into the caller's context, which should
    # be the the context of the class in which the decorator was called.
    local = sys._getframe(2).f_locals
    senderName = 'send%s%s' % (messageName[0].upper(), messageName[1:])
    sender = lambda self, stream, *args: messageObject.send(stream, *args)
    sender.__name__ = senderName
    for option in options:
        setattr(sender, '__%s' % option, options[option])
    local[senderName] = sender

def register(messageID, *signature, **options):
    def decorator(f):
        @wraps(f)
        def wrapper(self, *fargs):
            if not f(self, *fargs): 
                # Allow built-in protocol handlers to suppress passing of message to delegate 
                # by returning any True value
                self._handleCallback(f.__name__, *fargs)
        
        messageObject = _registerCallback(wrapper, messageID, signature, options)
        _addSenderMethod(f.__name__, messageObject, **options)
        return wrapper
    return decorator

class BinaryProtocol(object):
    def __init__(self, delegate, endianness='!'):
        self.delegate = delegate
        self._prefix = endianness # FIXME -- this should be overrideable and should be used to set the
                                  # prefix of all Messages.
        self.currentMessageID = None
        self._receivedAcks = set(self._pairedAcks.keys())
    
    def _handleCallback(self, name, *args):
        if self.delegate:
            callback = getattr(self.delegate, name, None)
            if callback is None:
                raise AttributeError('Protocol delegate %s has no handler for %s' % (self.delegate, name))
            
            callback(*args)
    
    def feed(self, stream):
        ''' Read one message from `stream`, blocking until the full message is read.  Consumes only one
            message, so if more than one message is available on the stream, caller will need to make
            multiple calls to `feed()`.
        '''
        try:
            data = stream.read(4)
            if not data:
                raise IOError("Connection reset by peer")
            messageID = struct.unpack('%si' % (self._prefix), data)[0]
            self.currentMessageID = messageID
            handler, message = self._handlers[messageID]
            args = message.receive(stream)
            if messageID in self._pairedAcks:
                self._receivedAcks.add(messageID)
            handler(self, *args)
        except KeyError, e:
            raise ProtocolError("Unexpected protocol message: %s" % (e))
        finally:
            self.currentMessageID = None


class ProtocolError(Exception):
    pass

__all__ = ('BinaryProtocol', 'BinaryMessage', 'register')