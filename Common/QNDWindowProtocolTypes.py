# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

WINDOW_CREATE_MSG_TYPE = 0x21
WINDOW_UPDATE_MSG_TYPE = 0x22
WINDOW_UNMAP_MSG_TYPE = 0x23
WINDOW_DESTROY_MSG_TYPE = 0x24

TAP_EVENT_MSG_TYPE = 0x1
TOUCH_DOWN_EVENT_MSG_TYPE = 0x2
TOUCH_MOVED_EVENT_MSG_TYPE = 0x3
TOUCH_UP_EVENT_MSG_TYPE = 0x4

ACK_IMAGE_MSG_TYPE = 0xf1

WINDOW_LIST_REQUEST_MSG_TYPE = 0x30
SUBSCRIBE_WINDOW_EVENTS_MSG_TYPE = 0x31 # Window create/destroy/etc events
UNSUBSCRIBE_WINDOW_EVENTS_MSG_TYPE = 0x32
SUBSCRIBE_WINDOW_MSG_TYPE = 0x33 # Window content/etc events
UNSUBSCRIBE_WINDOW_MSG_TYPE = 0x34

BEGIN_WINDOW_LIST_MSG_TYPE = 0x40
WINDOW_LIST_ITEM_MSG_TYPE = 0x41
END_WINDOW_LIST_MSG_TYPE = 0x42
