# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from Foundation import *
from AppKit import *
import objc

from collections import deque
from contextlib import closing
import select
import socket
import struct
from threading import Thread
import time

import jre.cocoa
import jre.debug

import traceback

from SocketConnection import SocketConnection
from IPhoneWindowProtocol import IPhoneWindowProtocol as WindowProtocol

class ImageSenderConnection(SocketConnection):
    def __init__(self, connection, address):
        super(ImageSenderConnection, self).__init__((connection, address), WindowProtocol(self), self)
        self._delegate = None
        
    def setDelegate_(self, delegate):
        self._delegate = delegate
        
    def socketConnectionClosed(self, connection):
        self._delegate.performSelectorOnMainThread_withObject_waitUntilDone_(
            self._delegate.lostSenderConnection_, connection, False)

    def socketConnectionError(self, connection, error):
        print "Lost sender connection: %s: %s" % (error.__class__.__name__, error)
        self.socketConnectionClosed(connection)
    
    def _remoteTapEvent(self, msgType, windowID, x, y):
        self._delegate.receiveRemoteTapEvent(msgType, windowID, x, y)
    
    def touchDownEvent(self, windowID, x, y):
        msgType = self._sc_currentMessageID
        self._remoteTapEvent(msgType, windowID, x, y)
    touchUpEvent = touchDownEvent
    touchMovedEvent = touchDownEvent
    
    def ackWindow(self, windowID):
        pass
    
    def requestWindowList(self):
        windowList = self._delegate.windows
        try:
            self.sendBeginWindowList()
            for windowID, windowController in windowList.items():
                if windowController.imageView and windowController.imageView.image():
                    w, h = windowController.imageView.image().size()
                else:
                    w, h = 0, 0
                self.sendWindowListItem(windowID, w, h)
        finally:
            self.sendEndWindowList()

    def subscribeToWindowEvents(self):
        for windowID, windowController in self._delegate.windows.items():
            w, h = windowController.imageView.image().size()
            self.sendCreateWindow(windowID, w, h)
        self._delegate.subscribeConnectionToWindowEvents(self)

    def unsubscribeFromWindowEvents(self):
        self._delegate.unsubscribeConnectionToWindowEvents(self)

    def subscribeToWindow(self, windowID):
        self._delegate.subscribeConnectionToWindow(self, windowID)

    def unsubscribeFromWindow(self, windowID):
        self._delegate.unsubscribeConnectionToWindow(self, windowID)
