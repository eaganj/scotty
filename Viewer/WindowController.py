# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

from ProxyWindow import ProxyWindow

class ScottyWindowController(NSWindowController):
    #imageView = objc.IBOutlet()
    
    def initFromNib(self): # No longer used
        self = super(ScottyWindowController, self).initWithWindowNibName_(u"ScottyWindow")
        if not self:
            return self
        
        return self
    
    def initWithDelegate_image_id_(self, delegate, image, windowID):
        window = ProxyWindow.alloc().initWithImage_(image)
        self.imageView = window.imageView
        self._delegate = delegate
        self._windowID = windowID
        self = super(ScottyWindowController, self).initWithWindow_(window)
        if not self:
            return self
            
        return self
    
    def receiveRemoteTapEvent(self, etype, x, y):
        self._delegate.receiveRemoteTapEvent(etype, self._windowID, x, y)
        
WindowController = ScottyWindowController