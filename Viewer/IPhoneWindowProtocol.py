# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from QNDWindowProtocol import *

class IPhoneWindowProtocol(QNDWindowProtocol):
    '''
    The iPhoneWindowProtocol is similar to the QNDWindowProtocol except that it adds an ack for window
    update messages to throttle down the broadcast rate of windows.
    '''
    @register(WINDOW_UPDATE_MSG_TYPE, 'i:windowID', 'o:windowBuffer', discard=True)
              # waitForAck=ACK_IMAGE_MSG_TYPE)
    def updateWindow(self, windowID, windowBuffer):
        pass
        
    @register(ACK_IMAGE_MSG_TYPE, 'i:windowID', ackFor=ACK_IMAGE_MSG_TYPE)
    def ackWindow(self, windowID):
        pass
        
    # ### FIXME FIXME:  These should really be inherited but the register mechanism breaks on inheritance!
    @register(WINDOW_CREATE_MSG_TYPE, 'i:windowID', 'i:width', 'i:height')
    def createWindow(self, windowID, width, height):
        pass
    
    @register(WINDOW_UNMAP_MSG_TYPE, 'i:windowID')
    def hideWindow(self, windowID):
        pass
    
    @register(WINDOW_DESTROY_MSG_TYPE, 'i:windowID')
    def destroyWindow(self, windowID):
        pass

    @register(TOUCH_DOWN_EVENT_MSG_TYPE, 'i:windowID', 'f:x', 'f:y')
    def touchDownEvent(self, windowID, x, y):
        pass

    @register(TOUCH_UP_EVENT_MSG_TYPE, 'i:windowID', 'f:x', 'f:y')
    def touchUpEvent(self, windowID, x, y):
        pass

    @register(TOUCH_MOVED_EVENT_MSG_TYPE, 'i:windowID', 'f:x', 'f:y')
    def touchMovedEvent(self, windowID, x, y):
        pass
    
    @register(WINDOW_LIST_REQUEST_MSG_TYPE)
    def requestWindowList(self):
        pass

    @register(BEGIN_WINDOW_LIST_MSG_TYPE)
    def beginWindowList(self):
        pass

    @register(WINDOW_LIST_ITEM_MSG_TYPE, 'i:windowID', 'i:width', 'i:height')
    def windowListItem(self, windowID, width, height):
        pass

    @register(END_WINDOW_LIST_MSG_TYPE)
    def endWindowList(self):
        pass

    @register(SUBSCRIBE_WINDOW_EVENTS_MSG_TYPE)
    def subscribeToWindowEvents(self):
        pass

    @register(UNSUBSCRIBE_WINDOW_EVENTS_MSG_TYPE)
    def unsubscribeFromWindowEvents(self):
        pass

    @register(SUBSCRIBE_WINDOW_MSG_TYPE, 'i:windowID')
    def subscribeToWindow(self, windowID):
        pass

    @register(UNSUBSCRIBE_WINDOW_MSG_TYPE, 'i:windowID')
    def unsubscribeFromWindow(self, windowID):
        pass