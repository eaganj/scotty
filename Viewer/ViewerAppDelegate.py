# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

from ImageReceiver import SVImageReceiver
from ImageSenderServer import ImageSenderServer
from WindowController import WindowController

class ViewerAppDelegate(NSObject):
    statusLabel = objc.IBOutlet()
    imageView = objc.IBOutlet()
    windows = objc.ivar()
    
    def awakeFromNib(self):
        self._imageReceiver = SVImageReceiver(self)
        self._imageReceiver.start()
        self._imageServer = ImageSenderServer(self)
        self._imageServer.start()
        self.windows = {}
        self.windowSubscribers = {}
        self.windowEventSubscribers = set()
    
    def receivedImageData_(self, (windowID, data)):
        image = NSImage.alloc().initWithData_(data)
        window = self.windows.get(windowID, None)
        if not window:
            window = WindowController.alloc().initWithDelegate_image_id_(self, image, windowID)
            window.showWindow_(self)
            self.windows[windowID] = window
            w, h = image.size()
            
            for connection in self.windowEventSubscribers:
                connection.sendCreateWindow(windowID, w, h)
        
        window.imageView.setImage_(image)
        self._imageServer.addWindow((windowID, data))

    def receivedHideWindow_(self, windowID):
        window = self.windows.get(windowID, None)
        if window:
            del self.windows[windowID]
            window.close()
            
            windowSubscribers = self.windowSubscribers.get(windowID, set())
            connections = self.windowEventSubscribers | windowSubscribers
            for connection in connections:
                connection.sendHideWindow(windowID)
                
        
    def subscribeConnectionToWindow(self, connection, windowID):
        self.windowSubscribers.setdefault(windowID, set()).append(connection)
    
    def unsubscribeConnectionToWindow(self, connection, windowID):
        self.windowSubscribers[windowID].remove(connection)
    
    def subscribeConnectionToWindowEvents(self, connection):
        self.windowEventSubscribers.add(connection)
    
    def unsubscribeConnectionToWindowEvents(self, connection):
        self.windowEventSubscribers.remove(connection)
    
    def lostReceiverConnection_(self, connection):
        for windowID in self.windows.keys():
            self.receivedHideWindow_(windowID) # FIXME: should really simulate closing/unmapping window
    
    def lostSenderConnection_(self, connection):
        self._imageServer.removeConnection(connection)
        if connection in self.windowEventSubscribers:
            self.windowEventSubscribers.remove(connection)
        
        empty = []
        for windowID, subscribers in self.windowSubscribers.items():
            if connection in subscribers:
                subscribers.remove(connection)
                if not subscribers:
                    empty.append(windowID)
        for windowID in empty:
            del self.windowSubscribers[windowID]
    
    def receiveRemoteTapEvent(self, etype, windowID, x, y):
        self._imageReceiver.receiveRemoteTapEvent(etype, windowID, x, y)
    