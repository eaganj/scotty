# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from Foundation import *
from AppKit import *
import objc

import hashlib

import jre.cocoa
import jre.debug

from SocketConnection import SocketConnection
from QNDWindowProtocol import QNDWindowProtocol as WindowProtocol
from QNDWindowProtocolTypes import *

class SVImageReceiverConnection(SocketConnection):
    def __init__(self, connection, address):
        SocketConnection.__init__(self, (connection, address), WindowProtocol(self), self)
        self._delegate = None
    
    def setDelegate_(self, delegate):
        self._delegate = delegate
        
    def socketConnectionClosed(self, connection):
        self._delegate.performSelectorOnMainThread_withObject_waitUntilDone_(
            self._delegate.lostReceiverConnection_, connection, False)
    
    def socketConnectionError(self, connection, error):
        print "Lost receiver connection: %s: %s" % (error.__class__.__name__, error)
        self.socketConnectionClosed(connection)
    
    def updateWindow(self, windowID, data):
        imdata = NSData.dataWithData_(buffer(data))
        #datahash = hashlib.md5(data).hexdigest()
        #imdatahash = hashlib.md5(imdata).hexdigest()
        #h = datahash if datahash == imdatahash else '%s != %s' % (datahash, imdatahash)
        # print '... Received', len(imdata), 'bytes', hashlib.md5(imdata).hexdigest()
        self._delegate.performSelectorOnMainThread_withObject_waitUntilDone_(
                self._delegate.receivedImageData_, (windowID, imdata), False)
    
    def hideWindow(self, windowID):
        self._delegate.performSelectorOnMainThread_withObject_waitUntilDone_(
                self._delegate.receivedHideWindow_, windowID, False)
    
    def sendRemoteTapEvent(self, etype, windowID, x, y):
        method = getattr(self, _eventMap[etype])
        method(windowID, x, y)

_eventMap = { TOUCH_DOWN_EVENT_MSG_TYPE: 'sendTouchDownEvent',
              TOUCH_UP_EVENT_MSG_TYPE: 'sendTouchUpEvent',
              TOUCH_MOVED_EVENT_MSG_TYPE: 'sendTouchMovedEvent',
            }