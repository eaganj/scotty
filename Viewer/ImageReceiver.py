# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

from Foundation import *
from AppKit import *
import objc

import pybonjour
import select
import socket
import struct
import sys
from threading import Thread
import time

import jre.cocoa
import jre.debug

from ImageReceiverConnection import SVImageReceiverConnection

DEFAULT_HOST = '0.0.0.0'
DEFAULT_PORT = 9888

class SVImageReceiver(Thread):
    def initWithDelegate_(self, delegate):
        #self = super(SVImageReceiver, self).init()
        super(SVImageReceiver, self).__init__()
        if not self:
            return self
        
        self._delegate = delegate
        
        self._initSocket()
        self._socket.listen(1)
        
        self._pool = [] # worker thread pool
        
        return self
    initWithServer_ = initWithDelegate_ # FIXME for backwards compat
    __init__ = lambda self, delegate: self.initWithDelegate_(delegate) and None
    
    def _initSocket(self):
        try:
            for attempt in xrange(24): # 24 tries at 5 seconds each == 2 minutes.
                try:
                    self._socket = socket.socket()
                    self._socket.bind((DEFAULT_HOST, DEFAULT_PORT))
                    break
                except socket.error, e:
                    if e.errno == 48:
                        if attempt == 0:
                            print "*** Press Ctrl-C to cancel. ***"
                        print "[%2s] Waiting for port %s on %s ... (%s)" % (attempt + 1,
                                                                            DEFAULT_PORT, 
                                                                            DEFAULT_HOST, 
                                                                            e.strerror)
                        time.sleep(5.0)
                    else:
                        raise
            else:
                print "Giving up."
            
            self._advertiseViaBonjour()
        except KeyboardInterrupt, e:
            sys.exit(u"Shutting down (cancelled by user)...")

    def _advertiseViaBonjour(self):
        def register_callback(sdRef, flags, errorCode, name, regtype, domain):
            if errorCode == pybonjour.kDNSServiceErr_NoError:
#                 print """Registered Service:
#     name    = %s
#     regtype = %s
#     domain  = %s""" % (name, regtype, domain)
                print "Advertised", name, "on", regtype, "in", domain, "via Bonjour"
    
        txtRecord = pybonjour.TXTRecord()
        txtRecord["path"] = '/~eaganj/'
        
        sdRef = pybonjour.DNSServiceRegister(name="",
                                             regtype="_scotty._tcp",
                                             port=DEFAULT_PORT,
                                             txtRecord=txtRecord,
                                             callBack=register_callback)
        
        try:
            for i in range(3): # Take a second and a half to figure things out.
                ready = select.select([sdRef], [], [], 0.5)
                if sdRef in ready[0]:
                    pybonjour.DNSServiceProcessResult(sdRef)
        except KeyboardInterrupt, e:
            sys.exit(u"Shutting down (cancelled by user)...")
    
    @jre.cocoa.threadsafe
    def run(self):
        while True:
            try:
                connection, address = self._socket.accept()
                #worker = SVImageReceiverConnection.alloc().initWithConnection_address_(connection, address)
                worker = SVImageReceiverConnection(connection, address)
                worker.setDelegate_(self._delegate)
                worker.start()
                self._pool.append(worker)
            
                deadWorkers = [ worker for worker in self._pool if not worker.isAlive() ]
                for worker in deadWorkers:
                    self._pool.remove(worker)
            except:
                jre.debug.printStackTrace()
    
    def receiveRemoteTapEvent(self, etype, windowID, x, y):
        for worker in self._pool:
            worker.sendRemoteTapEvent(etype, windowID, x, y)
