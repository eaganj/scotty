# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from AppKit import *
from Foundation import *
import objc

import jre.debug
import jre.geom

class ScottyProxyWindowView(NSView):
    def initWithFrame_(self, frame):
        self = super(ScottyProxyWindowView, self).initWithFrame_(frame)
        if not self:
            return self
        
        self._init()
        
        return self
    
    def initWithCoder_(self, coder):
        self = super(ScottyProxyWindowView, self).initWithCoder_(coder)
        if not self:
            return self
        
        self._init()
        
        return self
    
    def _init(self):
        self._image = None
        self._shouldRedrawShadow = False
    
    def image(self):
        return self._image
    
    def setImage_(self, image):
        if not image:
            print "*** Warning: ignoring null image"
            return
            
        if (self._image and self._image.size() != image.size()) or not self._image:
            self._updatedImage_(image)
            
        self._image = image
        self.setNeedsDisplay_(True)
    
    def drawRect_(self, rect):
        if not self._image:
            return
        
        NSColor.clearColor().set()
        NSRectFill(self.frame())
        
        path = NSBezierPath.bezierPath()
        rect = self.frame()
        radius = 5
        inset = NSInsetRect(rect, radius, radius)
        
        path.moveToPoint_((NSMinX(rect), NSMinY(rect)))
        path.lineToPoint_((NSMaxX(rect), NSMinY(rect)))
        path.appendBezierPathWithArcWithCenter_radius_startAngle_endAngle_((NSMaxX(inset), NSMaxY(inset)), radius, 0, 90)
        path.appendBezierPathWithArcWithCenter_radius_startAngle_endAngle_((NSMinX(inset), NSMaxY(inset)), radius, 90, 180)
        path.closePath()
        NSColor.blackColor().set()
        path.fill()
        
        scaledRect = jre.geom.scaleRectToFitInRect(rect, self.frame())
        self._image.drawInRect_fromRect_operation_fraction_(scaledRect,
                                                            NSMakeRect(0, 0, *self._image.size()),
                                                            NSCompositeSourceIn,
                                                            1.0)
        
        # Draw proxy indicator (dashed line)
        NSColor.redColor().set()
        path.setLineDash_count_phase_((5, 2), 2, 0)
        path.stroke()
        
        if self._shouldRedrawShadow:
            # Force window to update its shadow in case our shape has changed.
            window = self.window()
            if window.hasShadow():
                window.setHasShadow_(False)
                window.setHasShadow_(True)
            
            self._shouldRedrawShadow = False
    
    def _updatedImage_(self, image):
        if image:
            self.window().setDesiredFrameSize_(image.size())
            # self.setFrameSize_(self._image.size())
            # winFrame = self.window().frame()
            # winFrame = NSMakeRect(winFrame.origin.x, winFrame.origin.y, self._image.size().width, self._image.size().height)
            # print 'Changed winFrame from (%s x %s) to (%s x %s)' % (self.window().frame().size.width, self.window().frame().size.height, winFrame.size.width, winFrame.size.height)
            # #self.window().setFrame_display_animate_(winFrame, True, True)
            # self.window().setFrame_display_(winFrame, True)
        self._shouldRedrawShadow = True
        

ProxyWindowView = ScottyProxyWindowView