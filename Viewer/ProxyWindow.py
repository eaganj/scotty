# -*- coding: utf-8 -*-
#
# Scotty -- a meta-toolkit for runtime toolkit overloading
#
# Copyright 2009-2011, Université Paris-Sud
# by James R. Eagan (code at my last name dot me)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# and GNU Lesser General Public License along with this program.  
# If not, see <http://www.gnu.org/licenses/>.

from AppKit import *
from Foundation import *
import objc

import jre.debug
import jre.geom

from ProxyWindowView import ProxyWindowView
from QNDWindowProtocolTypes import *

class ScottyProxyWindow(NSWindow):
    def initWithImage_(self, image):
        self = super(ScottyProxyWindow, self).initWithContentRect_styleMask_backing_defer_(
                                                NSMakeRect(0, 0, *image.size()),
                                                NSBorderlessWindowMask, 
                                                NSBackingStoreBuffered, 
                                                True)
        if not self:
            return self
        
        self._init()
        
        return self
    
    
    def _init(self):
        self.imageView = ProxyWindowView.alloc().initWithFrame_(
                                                    self.contentRectForFrameRect_(self.frame()))
        self.setContentView_(self.imageView)
        self.setHasShadow_(True)
        self.setOpaque_(False)
        
        self._draggingWindow = False
        self._draggingOffset = None
        self._lastMouseEventPoint = None
    
    
    def setDesiredFrameSize_(self, size):
        frame = self.frame()
        newFrame = NSMakeRect(frame.origin.x, frame.origin.y,
                              size.width, size.height)
        self.imageView.setFrameSize_(size)
        self.setFrame_display_animate_(newFrame, True, True)
    
    ### Event handling ###
    
    def mouseDown_(self, event):
        location = self.convertBaseToScreen_(event.locationInWindow())
        self._lastMouseEventPoint = location
        if self._pointInTitlebar_(location):
            self._draggingWindow = True
            self._draggingOffset = jre.geom.delta(self.frame().origin, location)
        else:
            self._relayMouseEvent_(event)
    
    def mouseUp_(self, event):
        try:
            if not self._draggingWindow:
                self._relayMouseEvent_(event)
        finally:
            self._draggingWindow = False
            self._draggingOffset = None
            self._lastMouseEventPoint = None
    
    def mouseDragged_(self, event):
        if self._draggingWindow:
            # Get current mouse location, not event location, which might be out of date.
            location = NSEvent.mouseLocation() 
            frame = self.frame()
            dx, dy = self._draggingOffset
            self.setFrameOrigin_((location.x - dx, location.y - dy))
            self._lastMouseEventPoint = location
        else:
            self._relayMouseEvent_(event)
        
    def _pointInTitlebar_(self, point):
        frame = self.frame()
        return NSMouseInRect(point, 
                             NSMakeRect(frame.origin.x + 86, frame.origin.y + frame.size.height - 23,
                                        frame.size.width - 86, 23), 
                             False)
    
    def _relayMouseEvent_(self, event):
        if not self.windowController():
            return
            
        etype = _eventMap[event.type()]
        if not etype:
            NSLog(u"Ignoring unknown mouse event type: %s" % event.type())
        x, y = event.locationInWindow()
        self.windowController().receiveRemoteTapEvent(etype, x, y)

_eventMap = {
                NSLeftMouseDown: TOUCH_DOWN_EVENT_MSG_TYPE,
                NSLeftMouseUp: TOUCH_UP_EVENT_MSG_TYPE,
                NSRightMouseDown: None, # TODO
                NSRightMouseUp: None, # TODO
                NSOtherMouseDown: None, # TODO
                NSOtherMouseUp: None, # TODO
                NSMouseMoved: None, # Unmapped
                NSLeftMouseDragged: TOUCH_MOVED_EVENT_MSG_TYPE,
                NSRightMouseDragged: None, # TODO
                NSOtherMouseDragged: None, # TODO
                NSMouseEntered: None, # TODO
                NSMouseExited: None, # TODO
            }

ProxyWindow = ScottyProxyWindow