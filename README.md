Scotty — Runtime Toolkit Overloading
====================================

The Scotty meta-toolkit.  Scotty is a toolkit designed to support runtime toolkit overloading.  For more details, see the [UIST 2011 article][uist11].

[uist11]: http://dx.doi.org/10.1145/2047196.2047226

Using Scotty
============

Scotty is currently undocumented.  Good luck!  In the mean time, you can poke around the existing Instruments for examples.

The easiest way to install is to use [this download script][scotty-install].  It will clone the public repository for Scotty and all its dependencies:

* [jre-lib][]
* [Python State Machines][]
* [The Instrumental Interaction Toolkit][IIKit]
* [Instruments][]

It will then add whatever directory you run the script from to your Python path.  **Warning: it is *strongly* recommended that you run this command from a subfolder of your development sandbox that you intend to use only for Scotty**.

If you have not yet installed [SIMBL][], do so now.

To install Scotty, you will then need to run `make dev` from the `Scotty/Injector` directory.

[SIMBL]: http://www.culater.net/dl/files/SIMBL-0.9.9.zip
[jre-lib]: http://code.eagan.me/jre
[Python State Machines]: http://code.eagan.me/state-machines
[IIKit]: http://code.eagan.me/iikit
[Instruments]: http://code.eagan.me/instruments
[scotty-install]: http://code.eagan.me/scotty/downloads/scotty-checkout.sh

License
=======

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.